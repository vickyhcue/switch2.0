'use strict';

var switchApp=angular.module('switch', [
  'ngRoute',
  'switch.home',
  'switch.login',
  'switch.challenges',
  'switch.challenge',
  'switch.activities',
  'switch.rewards',
  'switch.videos',
  'switch.comingsoon',
  'switch.leaderboard',
  'switch.profile',
  'switch.feedback',
  'switch.register',
  'switch.settings',
  'switch.help',
  'switch.comments',
  'switch.video',
  'switch.team',
  'switch.menu',
  'switch.follow',
  'switch.tour',
  'switch.ilikeiwish',
  'ngCookies',
  'ngAlertify'
])
.constant('CONFIG', {
    environment: "dev", //  "dev"/"staging"/"prod"
    //apiKey:"bcab9a60-f60d-11e7-89ae-07ef69c05393",//accenture hcue
    //apiKey: "b64d3880-f69a-11e7-b58e-33dfd635587a",//accenture //WAVE1
    //apiKey:"92b85ea0-05c6-11e8-9048-65efe3add6b9",//TC-EDGE
    //apiKey:"5f11ede0-1170-11e8-8222-75dd07004e42",//local
    apiKey:"978fd9825197431fa90cc7c71486ca8e",//switch-prod
    // bundleid:63,//TC-EDGE
    // bundleid:"47",//accenture hcue
    // bundleid: "49",//accenture//WAVE1
    // bundleid: "1",//local
    bundleid: "1",//switch-prod
  switchUrl:"https://s3-ap-southeast-1.amazonaws.com/static.switch.petronas.com/SWITCH_STORE/index.html",
  latestActivityImg:"https://s3-ap-southeast-1.amazonaws.com/static.switch.petronas.com/Latest_Activity_Box/All_Content_Complete.jpeg",
	watchId:1,
	learnId:2,
	challengeId:3,
  feedbackRating:218,
  feedbackTopic:220,
  feedbackComment:243,
  iLikeChallenge:281
})
.run(['$location','$rootScope','appCookieFactory', function($location,$rootScope,appCookieFactory) {
	
	$rootScope.$on("$locationChangeStart",function(event,next,current){
		if(!((current.indexOf('#/video')>0 && next.indexOf('#/video')>0) ||
       (current.indexOf('#/challenge')>0 && next.indexOf('#/challenge')>0))){
      appCookieFactory.clearValue('switchChallengePosition');
      appCookieFactory.clearValue('switchVideoPosition');
    }
    // if(current==next && current.indexOf('#/challenge')>0){
    //   $location.path('/challenges');
	    // }
		if (next.indexOf('#/register') > -1 || next.indexOf('#/help') > -1 || next.indexOf('#/password') > -1) {
		    //$location.path('/register');
		}
		else if (!appCookieFactory.getValue('switchUserInfo')) {
			$location.path('/login')
		}
	})
}])



