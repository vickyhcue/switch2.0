'use strict';

angular.module('switch.activities', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/activities', {
    templateUrl: 'app/components/activities/activities.html',
    controller: 'activitiesController'
  });
}])

.controller('activitiesController', ['$scope','$document','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$document,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {
	
	//Load More
	$scope.loadMoreActivities=function(){
		getActivities(++$scope.currentActivityPage);
	}
	$scope.loadMorePhotoActivities=function(){
		getPhotoActivities(++$scope.currentPhotoActivityPage);
	}
	$scope.loadMoreUserPhotoActivities=function(){
		getUserPhotoActivites($scope.userPhotoId,++$scope.currentUserPhotoPage);
	}
	$scope.nextPageComments=function(index,id,pageNo){
		if($scope.showUserPhotos){
		    $scope.getActivityComments(index, id, ++$scope.currentPhotoActivity.pageNo, 'Loadmore');
		}
		else{
		    $scope.getActivityComments(index, id, ++$scope.activities[index].pageNo, 'Loadmore');
		}
	}

	//Go To User Profile Page
	$scope.gotoProfile=function(id){
		$location.path('/profile').search('id',id);
	}

	//Show User Photos
	$scope.showUserPhotoActivity=function(index,id,currentPhotoActivity){
		$scope.showUserPhotos=true;
		$scope.currentUserPhotoPage=1;
		$scope.userPhotos=[];
		$scope.showMoreUserPhotos=false;
		$scope.currentPhotoActivity=currentPhotoActivity;
		$scope.currentPhotoActivity.index=index;
		$scope.currentPhotoActivity.comments=[];
		$scope.currentPhotoActivity.more=false;
		$scope.currentPhotoActivity.pageNo=1;
		if(document.getElementById('photoFeed'))
			document.getElementById('photoFeed').className="collapse";
		getUserPhotoActivites(id,$scope.currentUserPhotoPage);
	}

	//Show Activity Feed
	$scope.showActivityFeed=function(){
		$scope.showUserPhotos=false;
		return false;
	}

	//Update Rating
	$scope.updateRate=function(index,id){

		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('rate')+"?session_key="+sessionkey+"&type=claim&type_id="+id;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				alertify.success('You have liked/unliked the post');
				if($scope.showUserPhotos){
				$scope.currentPhotoActivity.likeNo=data.likeNo;
				$scope.currentPhotoActivity.ratedLike=data.rated;
				}
				else{
				$scope.activities[index].likeNo=data.likeNo;
				$scope.activities[index].ratedLike=data.rated;
				}
			}
			else{
				alertify.error(data.error);

if(data.error=="Invalid session key"){

	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	
	}

	//Update Comment
	$scope.updateComment = function (index, id, content, challengeid, commentno) {
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		if(content==undefined || content==null){
			alertify.error('Please enter text to post a comment');
			return false;
		}
		//content=encodeURIComponent(content);
		var secretKey = CONFIG.apiKey;
		var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
		var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
		var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
		$scope.sessionkey = sessionkey;
		var paramdata = {
		    "userid": userId,
		    "bundleid": gameId,
		    "challengeid": challengeid,
		    "activitytype": "CMT",
		    "commenttext": content,
		    "challengecompleted": true,
		    "createdby": userId,
		    "activityid": id
		}

		var url = appLinkFactory.getUrl('challengeComment');
		var req = httpRequestService.connect(url, 'POST', paramdata);
		req.success(function(data){
			if(data.code==200){
				alertify.success('Your comment has been posted');
				if($scope.showUserPhotos){
				$scope.currentPhotoActivity.commentNo = parseInt(commentno) + 1;
				$scope.currentPhotoActivity.comment="";
				$scope.currentPhotoActivity.pageNo=0;
				$scope.getActivityComments(index,id,0);
				//document.getElementById('photoFeed').className="collapse";
				}
				else{
				$scope.activities[index].commentNo = parseInt(commentno) + 1;
				$scope.activities[index].comment="";
				$scope.activities[index].pageNo=0;
				$scope.getActivityComments(index,id,0);
				//document.getElementById('activityFeed'+index).className="collapse";
				}

			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	
	}

	$scope.getValues=function(page,type){
		$scope.search.filter=true;
		$scope.currentActivityPage=0;
		getActivities(page);
	}

	//Activity Comments
	$scope.getActivityComments = function (index, id, pageNo, fromval) {
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    if (fromval != 'Loadmore') {
	        pageNo = 0;
	        if (!$scope.showUserPhotos)
	            $scope.activities[index].pageNo = 0;
	        else
	            $scope.currentPhotoActivity.pageNo = 0;
	    }
		if(pageNo==undefined){
			pageNo=0;
			if(!$scope.showUserPhotos)
				$scope.activities[index].pageNo=0;
			else
				$scope.currentPhotoActivity.pageNo=0;
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url = appLinkFactory.getUrl('activitycommentlist').replace(new RegExp('{id}', 'g'), id) + "?pagesize=5&pagenumber=" + pageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				if($scope.showUserPhotos){
					if(!$scope.currentPhotoActivity.comments){
						$scope.currentPhotoActivity.comments=[];
					}
					else if(pageNo==0){
						$scope.currentPhotoActivity.comments=[];
					}
					$scope.currentPhotoActivity.comments=$scope.currentPhotoActivity.comments.concat(data.data);
					$scope.currentPhotoActivity.more=data.more;
				}
				else {
				    if (fromval != 'Loadmore') {
				        $scope.activities[index].comments = [];
				    }
					if(!$scope.activities[index].comments){
						$scope.activities[index].comments=[];
					}
					else if(pageNo==0){
						$scope.activities[index].comments=[];
					}
					$scope.activities[index].comments=$scope.activities[index].comments.concat(data.data);
					$scope.activities[index].more=data.more;					
				}
			}
			else{
				if($scope.showUserPhotos){
					$scope.currentPhotoActivity.comments=[];
					$scope.currentPhotoActivity.more=[];
				}
				else{
					$scope.activities[index].comments=[];
					$scope.activities[index].more=false;
				}
			}
			
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}		

	//Activities
	var getActivities=function(pgNo){
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var url = appLinkFactory.getUrl('activities').replace(new RegExp('{id}', 'g'), gameId) + "?user_id=" + userId + "&pagesize=10&pagenumber=" + $scope.currentActivityPage;
		if($scope.search.restrict=="Following"){
			url+="&type=following";
		}
		if ($scope.search.type == "user") {
		    url += "&keywords_type=user";
		}
		if ($scope.search.type == "comment") {
		    url += "&keywords_type=comment";
		}
		if ($scope.search.type == "challenge") {
			url+="&keywords_type=challenge";
		}
		if($scope.search.keyword!=""){
			url+="&keywords="+encodeURIComponent($scope.search.keyword);
		}
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				if($scope.search.filter && pgNo==0){
					$scope.activities=[];
				}
				$scope.activities=$scope.activities.concat(data.data);
				$scope.showMoreActivity=data.more;
			}
			else{
				$scope.activities=[];
				$scope.showMoreActivity=false;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	var getPhotoActivities=function(pgNo){
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var url = appLinkFactory.getUrl('activities').replace(new RegExp('{id}', 'g'), gameId) + "?filter=image&user_id=" + userId + '&filteruser=false' + "&pagesize=10&pagenumber=" + $scope.currentPhotoActivityPage;
	    var req = httpRequestService.connect(url, 'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.photoActivities=$scope.photoActivities.concat(data.data);
				$scope.showMorePhotoActivity=data.more;
			}
			else{
				$scope.photoActivities=[];
				$scope.showMorePhotoActivity=false;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//User Photo Activities
	var getUserPhotoActivites=function(id,pgNo){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
		$scope.userPhotoId = id;
		var url = appLinkFactory.getUrl('imageactivity').replace(new RegExp('{id}', 'g'), gameId) + "?filter=image&user_id=" + id + '&filteruser=true';
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.userPhotos=$scope.userPhotos.concat(data.data);
				$scope.showMoreUserPhotos=data.more;
			}
			else{
				$scope.userPhotos=[];
				$scope.showMoreUserPhotos=false;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	$scope.getDate = function (Visitedate) {
	    if (Visitedate !== undefined) {
	        var m = moment(new Date(), "YYYYMMDD");
	        var date = moment(Visitedate, "YYYYMMDD HH:mm");


	        var years = m.diff(date, 'years');
	        m.add(-years, 'years');
	        var months = m.diff(date, 'months');
	        m.add(-months, 'months');
	        var days = m.diff(date, 'days');
	        m.add(-days, 'days');
	        var hours = m.diff(date, 'hours');
	        m.add(-hours, 'hours');
	        var minutes = m.diff(date, 'minutes');
	        var daysago;
	        if (years <= 0) {
	            if (months <= 0) {
	                if (days <= 0) {
	                    if (hours == 0) {
	                        daysago = minutes.toString() + " minutes ago ";
	                    }
	                    else {
	                        daysago = hours.toString() + " Hours " + minutes.toString() + " minutes ago ";
	                    }

	                } else {
	                    daysago = days.toString() + " Day(s) Ago ";
	                }
	            } else {
	                if (months > 1) {
	                    daysago = months.toString() + " Months " + days.toString() + " Day(s) " + " ago ";
	                } else {
	                    daysago = months.toString() + " Month " + days.toString() + " Day(s)" + " ago ";
	                }
	            }
	        } else {
	            daysago = $filter('date')(new Date(Visitedate), "dd-MMM-yyyy");
	        }
	        return daysago;
	    }
	    else {

	    }
	}

	var init=function(){
		$scope.activities=[];
		$scope.photoActivities=[];
		$scope.userPhotos=[];
		$scope.currentActivityPage=0;
		$scope.currentPhotoActivityPage=0;
		$scope.currentUserPhotoPage=1;
		$scope.showMoreActivity=false;
		$scope.showMorePhotoActivity=false;
		$scope.showUserPhotos=false;
		$scope.showMoreUserPhotos=false;
		$scope.search={restrict:'Following',type:'User',keyword:'',filter:false,showtype:false,showfilter:false};
		getActivities($scope.currentActivityPage);
		getPhotoActivities($scope.currentPhotoActivityPage);
	}
	$document.on('click', function(event) {
          if($scope.search.showtype && !angular.element(event.target).hasClass('search-dropdown')){
            $scope.search.showtype=false;
            $scope.$apply();
          }
          else if($scope.search.showfilter && !angular.element(event.target).hasClass('search-filter')){
            $scope.search.showfilter=false;
            $scope.$apply();
          }
    });
	init();
}]);