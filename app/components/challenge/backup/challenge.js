'use strict';

angular.module('switch.challenge', ['ngRoute','ngAlertify'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/challenge', {
    templateUrl: 'app/components/challenge/challenge.html',
    controller: 'challengeController'
  });
   $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])


.controller('challengeController', ['$scope','$window','$location','$sce','$timeout','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$window,$location,$sce,$timeout,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {

	$scope.trustSrc = function(src) {
    	return $sce.trustAsResourceUrl(src);
  	}
	
  	$scope.refresh=function(){
  		$window.location.reload()
  	}

	var challengeId;

	//Select a claim in challenge
	$scope.selectClaim=function(id,isSingle){
		if(isSingle){
			$scope.quiz_option_id=id;
		}
		else{
			var ind=$scope.quiz_option_id_list.indexOf(id);
			if(ind>=0){
				$scope.quiz_option_id_list.splice(ind,1);
			}
			else{
				$scope.quiz_option_id_list.push(id);
			}
		}
	}

	//trigger photo
	$scope.uploadPhoto=function(){
		document.getElementById('challengePhoto').click();
	}

	// //read photo
	// $scope.$watch('file', function(newfile, oldfile) {
 //      if(angular.equals(newfile, oldfile) ){
 //        return;
 //      }

 //      console.log(newfile);
 //    });

	//Comment a challenge
	$scope.commentChallenge=function(){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengeComment')+"?session_key="+sessionKey+"&secret_key="+CONFIG.apiKey+"&type=challenge&type_id="+challengeId+"&content="+$scope.challenge.commentText;
		var req=httpRequestService.connect(url,'POST');
		req.success(function(data){
			if(data.code==200){
				//$scope.challenge=data;
				$scope.comments=[];
				$scope.commentsPageNo=1;
				getChallengeComments(challengeId,1);
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Load More Comments
	$scope.nextPageComments=function(){
		$scope.commentsPageNo+=1;
		getChallengeComments(challengeId,$scope.commentsPageNo);
	}

	//Load More Claims
	$scope.nextPageClaims=function(){
		$scope.claimsPageNo+=1;
		getChallengeClaims(challengeId,$scope.claimsPageNo);
	}

	//Claim a challenge
	$scope.claimChallenge=function(){
		var data;
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('claimChallenge');
		switch($scope.challenge.challengeTypeId){
			case 1: 
					if($scope.response==undefined || $scope.response.comment==undefined || $scope.response.comment==""){
						alertify.error('Enter Comments to complete the challenge');
						return false;
					}
					if($scope.challenge.photoOnly && !$scope.response.filepreview){
						alertify.error('Upload an image to complete the challenge');
						return false;
					}
					
					
					url+="?secret_key="+CONFIG.apiKey+"&session_key="+sessionKey+"&challenge_id="+parseInt(challengeId)+"&comment="+$scope.response.comment;
					data={};
					break;
			case 2:
			case 10:
			case 11:
					if($scope.challenge.multiSelect ){
						var quiz_options=[];
						$scope.quiz_option_id_list.forEach(function(e,i){
							quiz_options.push({'id':e});
						})
						data={
							challenge_id: parseInt(challengeId), 
							secret_key: CONFIG.apiKey, 
							session_key: sessionKey, 
							quiz_option_id_list:JSON.stringify(quiz_options)
						};
					}
					else{
						data={
							challenge_id: parseInt(challengeId), 
							secret_key: CONFIG.apiKey, 
							session_key: sessionKey, 
							quiz_option_id: $scope.quiz_option_id
						};
					}
					break;
		}
	    var param = "";
	    var header;
	    if($scope.challenge.challengeTypeId==1){
	    	// header={'header':{}}
	    	// param=new FormData();
	    	// param.append('file',$scope.response.file);
	    	// param.append('filename','file_name');
	    	var data = new FormData();
			data.append("file", $scope.response.file);
			data.append("file_name", "sample.png");

			var xhr = new XMLHttpRequest();

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4 && (this.status==200||this.status==201)) {
			  	console.log(this);
			    data=JSON.parse(this.responseText);
			    if(data.code==200){
					$scope.challenge.completionMessage=(data.completionMessage!=undefined && data.completionMessage.length>0)?data.completionMessage:'';
						
					//alertify.success((data.completionMessage?data.completionMessage:'Completed Successfully')+' Points Earned:'+data.points);
					//$scope.challenge.completionMessage=(data.completionMessage?data.completionMessage:'Completed Successfully.');
					// if(data.points){
					// 	$scope.challenge.completionMessage+=' You have Earned: +'+data.points+' points';
					// }
					if(data.items){
						$scope.challenge.badges=data.items;
					}
					
					document.getElementById('confirmation').click();
					
					//$timeout(function(){$window.location.reload()},3000);
				}
				else{
					alertify.error('Please select an option to complete the challenge');
				}
			  }
			  else if(this.readyState===4){
			  	alertify.error(data.error)
			  }
			});

			xhr.open("POST", url);

			xhr.send(data);
	    }
	    else{
		    for (var key in data) {
		         if (data.hasOwnProperty(key)) {
		         		if(data[key]!=undefined)
		               		param+=(encodeURIComponent(key) + "=" + encodeURIComponent(data[key])) + "&";
		         }
		    }
		
	    
			var req=httpRequestService.connect(url,'POST',param);
			req.success(function(data){
				if(data.code==200){
					$scope.challenge.completionMessage=(data.completionMessage!=undefined && data.completionMessage.length>0)?data.completionMessage:'';
					
					//alertify.success(data.completionMessage?data.completionMessage:'Completed Successfully'+' Points Earned:'+data.points);
					//$timeout(function(){$window.location.reload()},3000);
					//$scope.challenge.completionMessage=(data.completionMessage?data.completionMessage:'Completed Successfully.');
					// if(data.points){
					// 	$scope.challenge.completionMessage+=' You have Earned: +'+data.points+' points';
					// }
					if(data.items){
						$scope.challenge.badges=data.items;
					}
					
					document.getElementById('confirmation').click();
					
				}
				else{
					alertify.error('Please select an option to complete the challenge');
				}			
			})
			req.error(function(err,msg){
				alertify.error('Please Check Internet Connectivity (or) No data is found');
			});
		}
	}

		//Update Rating
	$scope.updateRate=function(index,id){

		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('rate')+"?session_key="+sessionkey+"&type=claim&type_id="+id;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				alertify.success('You have liked/unliked the post');
				$scope.challengeClaims[index].likeNo=data.likeNo;
				$scope.challengeClaims[index].ratedLike=data.rated;
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	
	}

	//Update Comment
	$scope.updateComment=function(index,id,content){

		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('challengeComment')+"?session_key="+sessionkey+"&type=claim&type_id="+id+"&content="+content;
		var req=httpRequestService.connect(url,'POST');
		req.success(function(data){
			if(data.code==200){
				alertify.success('Your comment has been posted');
				
				$scope.challengeClaims[index].commentNo=data.commentNo;
				$scope.challengeClaims[index].comment="";
				document.getElementById('activityFeed'+index).className="collapse";
				
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	
	}


	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Game Points
	var getGamePoints=function(){
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid)+"?session_key="+sessionkey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Challenge
	var getChallenge=function(challengeId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challenge').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.challenge=data;
				if(!$scope.challenge.accepted){
					acceptChallenge(challengeId);
				}
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Accept a Challenge
	var acceptChallenge=function(challengeId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('acceptChallenge')+"?session_key="+sessionKey+"&type_id="+challengeId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			//Success
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Comments
	var getChallengeComments=function(challengeId,pageNo){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengeComments').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey+"&page="+$scope.commentsPageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.comments=$scope.comments.concat(data.data);
				$scope.showMoreComments=data.more;
			}
			else{
				$scope.comments=[];
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Claims
	var getChallengeClaims=function(challengeId,page){
		if(page==undefined){
			page=1;
		}
		if(page==1){
			$scope.challengeClaims=[];
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengeClaims').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey+"&page="+page;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.challengeClaims=$scope.challengeClaims.concat(data.data);
				$scope.showMoreClaims=data.more;
			}
			else{
				$scope.challengeClaims=[];
				$scope.showMoreClaims=false;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	var init=function(){
		$scope.comments=[];
		$scope.response={};
		$scope.quiz_option_id;
		$scope.quiz_option_id_list=[];
		$scope.commentsPageNo=1;
		$scope.claimsPageNo=1;
		challengeId=$location.search().id;
		if(!challengeId && challengeId==null)
			$location.path('/challenges');
		getGamePoints();
		getChallenge(challengeId);
		getChallengeComments(challengeId,$scope.commentsPageNo);		
		getChallengeClaims(challengeId,$scope.claimsPageNo);
		
	}
	
	init();
}]);