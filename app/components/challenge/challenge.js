'use strict';

angular.module('switch.challenge', ['ngRoute','ngAlertify','ngSanitize'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/challenge', {
    templateUrl: 'app/components/challenge/challenge.html',
    controller: 'challengeController'
  });
   $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])


.controller('challengeController', ['$scope','$window','$location','$sce','$timeout','$interval','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$window,$location,$sce,$timeout,$interval,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {

	$scope.trustSrc = function(src) {
    	return $sce.trustAsResourceUrl(src);
  	}
	
	  $scope.escapeHTML=function(text){
		//return text;
		return $sce.trustAsHtml(text);
	}
  	$scope.refresh=function(){
  		$window.location.reload()
  	}

	var challengeId;
	$scope.photocommenttext = "";
	//Go To User Profile Page
	$scope.gotoProfile=function(id){
		$location.path('/profile').search('id',id);
	}

	

	$scope.getDate = function (Visitedate) {
	    if (Visitedate !== undefined) {
	        var m = moment(new Date(), "YYYYMMDD");
	        var date = moment(Visitedate, "YYYYMMDD HH:mm");


	        var years = m.diff(date, 'years');
	        m.add(-years, 'years');
	        var months = m.diff(date, 'months');
	        m.add(-months, 'months');
	        var days = m.diff(date, 'days');
	        m.add(-days, 'days');
	        var hours = m.diff(date, 'hours');
	        m.add(-hours, 'hours');
	        var minutes = m.diff(date, 'minutes');
	        var daysago;
	        if (years <= 0) {
	            if (months <= 0) {
	                if (days <= 0) {
	                    if (hours == 0) {
	                        daysago =  minutes.toString() + " minutes ago ";
	                    }
	                    else {
	                        daysago = hours.toString() + " Hours " + minutes.toString() + " minutes ago ";
	                    }
	                    
	                } else {
	                    daysago = days.toString() + " Day(s) Ago ";
	                }
	            } else {
	                if (months > 1) {
	                    daysago = months.toString() + " Months " + days.toString() + " Day(s) " + " ago ";
	                } else {
	                    daysago = months.toString() + " Month " + days.toString() + " Day(s)" + " ago ";
	                }
	            }
	        } else {
	            daysago = $filter('date')(new Date(Visitedate), "dd-MMM-yyyy");
	        }
	        return daysago;
	    }
	    else {

	    }
	}

	//Select a claim in challenge
	$scope.selectClaim = function (id, isSingle, index,type) {
	    var ind;
		if(isSingle){
			if(type == "poll"){
				$scope.quiz_option_id_list=[];
				$scope.quiz_option_id_list.push(id);
			}
			else{
				$scope.quiz_option_id=id;
			}
			
		}
		else {
		    if (id.challengeType == "POLL CHALLENGE") {
		      
		        ind = $scope.quiz_option_id_list.indexOf(id.pollOptions.data[index].Answer);
		        if (ind >= 0) {
		            $scope.quiz_option_id_list.splice(ind,1);// id.pollOptions.data[index].Answer;
		        }
		        else {
		            $scope.quiz_option_id_list.push(id.pollOptions.data[index].Answer);
		        }
		    }
		    else {
		        angular.forEach($scope.quiz_option_id_list, function (i, e) {
		            if (i.id == id.id) {
		                ind = e;
		            }
		        })
		        //ind=$scope.quiz_option_id_list.indexOf(id);
		        if (ind >= 0) {
		            $scope.quiz_option_id_list[ind].value = id.answercollection[index].Answer;
		        }
		        else {
		            $scope.quiz_option_id_list.push({
		                id: id.id,
		                Answer: id.answercollection[index].Answer
		            });
		        }
		    }
		  
		}
	}

	$scope.showVideoChallenge = function(index){
		$scope.challenge.showVideo=true;
		$timeout(function(){
			document.getElementById('videoSource').play();
		},500);
	}

	//trigger photo
	//$scope.uploadPhoto=function(){
	//	document.getElementById('challengePhoto').click();
	//}
	$scope.uploadPhoto = function () {
	    document.getElementById('challengePhoto').click();
	    var preview = document.getElementById('challengePhoto');

	    var file = preview.files[0];
	    if (file == undefined) {
	        return;
	    }
	    var formData = new FormData();

	    formData.append("fileUpload", file);

	    var url = appLinkFactory.getUrl('FileUpload');

	    var uploadCheck = "UPLOAD";

	    var fileuploadreq = httpRequestService.connect(url, 'POST', formData, uploadCheck);



	    fileuploadreq.success(function (data) {
	        $scope.imageurlcomment = data.message;
	    })
	    fileuploadreq.error(function (err, msg) {
	        console.log('error');
	    })

	}
	// //read photo
	// $scope.$watch('file', function(newfile, oldfile) {
 //      if(angular.equals(newfile, oldfile) ){
 //        return;
 //      }

 //      console.log(newfile);
 //    });

	//Comment a challenge
	$scope.commentChallenge = function () {
	    if ($scope.challenge.commentText == "" || $scope.challenge.commentText == undefined) {
	        alertify.error('Please enter text to post a comment');
	        return false;
	    }

		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		var content=$scope.challenge.commentText;
		
		content=content;
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url=appLinkFactory.getUrl('challengeComment');
		var param={
			"userid":userId,
			"bundleid":gameId,
			"topicid":$scope.challenge.quest.id,
			"challengeid":$scope.challenge.id,
			"activitytype":"CMT",
			"activityid":null,
			"replyid":null,
			"commenttext":content,  
			"createdby":userId 
		}

		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
				$scope.comments=[];
				$scope.challenge.commentNo=parseInt($scope.challenge.commentNo)+1;
				$scope.challenge.commentText="";
				$scope.commentsPageNo=0;
				getChallengeComments(challengeId,1);
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Video Complete Challenge
	$scope.videoComplete=function(){
		if(!$scope.challenge.claimed&&$scope.challenge.quizOptions&&$scope.challenge.quizOptions.length==1){
			$scope.claimChallenge();
		}
		else if(!$scope.challenge.claimed && ($scope.challenge.quizOptions==undefined || ($scope.challenge.quizOptions!=undefined &&$scope.challenge.quizOptions.length>1))){
			//$scope.challenge.enableSubmit=true;
			$scope.challengeConfig.disableSubmitBtn=false;
			$scope.$apply();
		}
	}

	//Clear Blur
	$scope.clearBlur=function(){
		//$scope.challenge.blurText=false;
		if($scope.challengeConfig.blurText)
			$scope.claimChallenge();
	}

	$scope.clearPDFBlur=function(){
		// if($scope.challengeConfig.pdfLoaded){
		// 	if($('iframe').contents().find('#secondaryPresentationMode').is(':visible')){
		// 		$('iframe').contents().find('#secondaryPresentationMode').is(':visible').click();
		// 	}
		// 	else{
		// 		window.open($scope.challenge.medias[0].url,'_blank');
		// 		$scope.claimChallenge();
		// 	}
		// }
		document.getElementById('pdfopen').click();
		//$scope.challenge.blurText=true;
	}
	$scope.nextPageClaimComments=function(index,id,pageno){
		$scope.challengeClaims[index].pageNo=pageno;
		$scope.getActivityComments(index,id,++$scope.challengeClaims[index].pageNo);
	}

	//Load More Comments
	$scope.nextPageComments=function(){
		$scope.commentsPageNo+=1;
		getChallengeComments(challengeId,$scope.commentsPageNo);
	}

	//Load More Claims
	$scope.nextPageClaims=function(){
		$scope.claimsPageNo+=1;
		getChallengeClaims(challengeId,$scope.claimsPageNo);
	}

	//Claim a challenge
	$scope.claimChallenge=function(){
		if(event)
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		var data;
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url=appLinkFactory.getUrl('challengeComplete');
		var customcollection = [];
		$scope.mandatoryfield = false;
	    var header;		
	    var param = {
			"userid":userId,
			"challengeid":$scope.challenge.id,
			"activitytype":"COM",
			"commenttext":"",
			"challengecompleted":true,
			"isaccepted": true,
			"challengetypeid":$scope.challenge.challengeTypeId,
			"active": true,
			"pointsawarded": $scope.challenge.points,
			
	    };
	    if ($scope.challenge.challengeType == 'QUIZ CHALLENGE') {

	        param.isanswered = ($scope.quiz_option_id.isrightanswer == "") ? undefined : $scope.quiz_option_id.isrightanswer;
	        param.answertext = $scope.quiz_option_id.Answer;
	        param.pointsawarded = ($scope.quiz_option_id.isrightanswer == false) ? $scope.challenge.incorrectpoints : $scope.challenge.correctpoints;
	     
			//param.pointsawarded=$scope.quiz_option_id.points||0;
	    }
	    if ($scope.challenge.challengeType == 'POLL CHALLENGE') {
			if($scope.challenge.pollOptions.data.length == 1){
				param.answerjson = {
					 data: $scope.challenge.pollOptions.data
					};
			}
			else{
				if ($scope.quiz_option_id_list.length != 0) {
					param.answerjson = { data: $scope.quiz_option_id_list };
				}
				else {
					alertify.error("Please select the answer.");
					return;
				}
			}
	       
	    }
	    if ($scope.challenge.challengeType == 'PHOTO CHALLENGE') {
	       
	        if ($scope.imageurlcomment != undefined) {
	            param.propertyuploadinfo = $scope.imageurlcomment;
	            //param.commenttext = $scope.response.comment;
	        }
	        else {
	            alertify.error("Please upload photo and enter comments for this challenge.");
	            return;
	        }
	        
		}
		if ($scope.challenge.challengeType == 'STANDARD CHALLENGE') {
	       
	        if ($scope.imageurlcomment != undefined) {
	            param.propertyuploadinfo = $scope.imageurlcomment;	           
			}

			if($scope.response.comment != undefined && $scope.response.comment != ""){
				param.commenttext = $scope.response.comment;
			}
	        else {
	            alertify.error("Please enter comments for this challenge.");
	            return;
	        }
	        
		}
		if ($scope.challenge.challengeType == 'CUSTOM CHALLENGE') {
		    angular.forEach($scope.textboxcoll, function (item) {
		        if (item.Required == true && (item.Value == '' || item.Value == undefined)) {
		            $scope.mandatoryfield = true;
		        }
		        customcollection.push(item);
		    })
		    angular.forEach($scope.dropdowncoll, function (item) {
		        if (item.Required == true && (item.Value == '' || item.Value == undefined)) {
		            $scope.mandatoryfield = true;
		        }
		        customcollection.push(item);
		    })

		    if ($scope.mandatoryfield) {
		        alertify.error("Enter mandatory fields");
		        return;
		    }

		    if ($scope.imageurlcomment != undefined) {
		        param.propertyuploadinfo = $scope.imageurlcomment;
		    }
		    else if ($scope.imageurlcomment == undefined && $scope.customuploadphoto == true) {
		        alertify.error("Please upload photo for this challenge.");
		        return;
		    }

		    param.customjson = {
		        data: customcollection
		    };

		}
	    if ($scope.challenge.challengeType == 'QUIZ MULTIPLE QUESTION CHALLENGE') {
	        var ismandatorerror = 0;
	        var collectionCount = 0;
	        if ($scope.quiz_option_id_list.length != 0) {
	            angular.forEach($scope.challenge.quizOptions.data, function (val) {
	                if (val.ismandatory == true) {
	                    collectionCount = collectionCount + 1;
	                }
	                angular.forEach($scope.quiz_option_id_list, function (item) {
	                    if (val.ismandatory == true) {	                       
	                    if (val.id == item.id) {
	                        ismandatorerror = ismandatorerror + 1;
	                    }
	                }
	              })
	            })

	            if (collectionCount != ismandatorerror) {
	                alertify.error("Please answer the mandatory questions.");
	                return;
	            }
	            param.answerjson = {
	                data: $scope.quiz_option_id_list
	            };
	        }
	        else {
	            alertify.error("Please select the answer.");
	            return;
	        }
	        
	    }
	    console.log(param);
	var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
			if(data.code==200){
				$scope.challenge.completionMessage=(data.successmsg!=undefined && data.successmsg.length>0)?data.successmsg:'';
				$scope.popupmsg = "";
				if(data.items){
					$scope.challenge.badges=data.items;
				}
				else{
					$scope.challenge.badges=[];
				}
				$scope.completemsg = (data.successmsg == "") ? 'Congratulation, completed successfully' : data.successmsg;


				$scope.challengeResponse = data;

				if ($scope.challenge.challengeType == 'QUIZ CHALLENGE') {
				    if ($scope.quiz_option_id.isrightanswer == true) {
				        $scope.popupmsg = ($scope.challenge.completeonmsg == "") ? 'Congratulation, completed successfully' : $scope.challenge.completeonmsg;
				        $scope.challengeResponse.correctAnswer = true;
				    }
				    else {
				        $scope.popupmsg = ($scope.challenge.wrongmsg == "") ? 'Sorry, you have answered wrong' : $scope.challenge.wrongmsg;
				        $scope.challengeResponse.correctAnswer = false;
				    }
				}

				if ($scope.popupmsg == "") {
				    $scope.popupmsg = ($scope.challenge.completeonmsg == "") ? 'Congratulation, completed successfully' : $scope.challenge.completeonmsg;   
				}				
				$scope.challenge.commentText = "";
				document.getElementById('confirmation').click();
				
			}
			else{
				alertify.error('Please select an option to complete the challenge');
			}			
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
		
	}

	//Like a challenge
	$scope.postLike=function(){
		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('challengeActivity');
		var param={
			"userid": userId,
			"challengeid":$scope.challenge.id,
			"activitytype": "LIK",
			"createdby":userId,
			"active":$scope.challenge.ratedLike==false?true:false
		}
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
			//if(data.code==200){
				if(data.liked == true){

				$scope.challenge.likeNo=parseInt($scope.challenge.likeNo)+1;
					//alertify.success('You have liked the post');
				}
				else{

				$scope.challenge.likeNo=parseInt($scope.challenge.likeNo)-1;
					//alertify.success('You have unliked the post');
				}
				$scope.challenge.ratedLike=data.liked;
				
			// }
			// else{
			// 	alertify.error(data.error);
			// }
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

		//Update Rating
	$scope.updateRate=function(index,id){

		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('rate')+"?type=claim&type_id="+id;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				alertify.success('You have liked/unliked the post');
				$scope.challengeClaims[index].likeNo=data.likeNo;
				$scope.challengeClaims[index].ratedLike=data.rated;
			}
			else{
				alertify.error(data.error);
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	
	}

	//Update Comment
	$scope.updateComment=function(index,id,content){
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		if(content==undefined || content == null){
			alertify.error('Please enter text to post a comment');
			return false;
		}
		content=encodeURIComponent(content);
		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('challengeComment')+"?type=claim&type_id="+id+"&content="+content;
		var req=httpRequestService.connect(url,'POST');
		req.success(function(data){
			if(data.code==200){
				alertify.success('Your comment has been posted');
				
				$scope.challengeClaims[index].commentNo=data.commentNo;
				$scope.challengeClaims[index].comment="";
				document.getElementById('activityFeed'+index).className="collapse  col-lg-12";
				
			}
			else{
				alertify.error(data.error);
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	
	}


	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Game Points
	var getGamePoints=function(){
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid);
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Challenge
	var getChallenge=function(challengeId){
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var url=appLinkFactory.getUrl('challenge').replace(new RegExp('{id}','g'),challengeId)+"?bundleid="+gameId+"&user_id="+userId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
			    $scope.challenge = data;
			    $scope.customuploadphoto = data.mandatoryupload;
			    $scope.textboxcoll = [];
			    $scope.dropdowncoll = [];
			    if ($scope.challenge.challengeType == 'CUSTOM CHALLENGE') {
			        angular.forEach($scope.challenge.customjson.data, function (val) {
			            if (val.Type == '1') {
			                $scope.textboxcoll.push(val);
			            }
			            else {
			                $scope.dropdowncoll.push(val);
			            }
			        });
			    }
			   

				if($scope.challenge.footnote!=null)
				$scope.challenge.footnote=$scope.escapeHTML($scope.challenge.footnote.replace(/href="([0-9]*)"/g,'href="#/challenge?id='+'$1'+'"'));
				$scope.challenge.showVideo = false;
				allowChallenge(challengeId);
				if(!$scope.challenge.accepted){
				    $scope.challengecompletemethod();
				}
				challengeInit();
				//getChallengesInGame();
			}
			else{
				alertify.error('Please select an option to complete the challenge');;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Init
	var challengeInit=function(){
		//$scope.challenge.enableSubmit=false;
		//$scope.challenge.blurText=true;
		$scope.completedMessage="Challenge Completed";
		$scope.challengeConfig={
			
			'blurPdf':false,
			'blurText':false,
			'isSubmit':true,
			'pdfLoaded':false,
			'isIlike':false,
			'showSubmit':false
		};
		

		if($scope.challenge.challengeType=="PDF CHALLENGE" && !$scope.challenge.claimed){
			$scope.challengeConfig.blurPdf=true;		
		}
		if($scope.challenge.challengeType!="PDF CHALLENGE"){
			$scope.challengeConfig.showSubmit=true;		
		}
		if($scope.challenge.challengeType=="FLASHCARD"){
			$scope.challengeConfig.isSubmit=false;
		}
		
	}

	//Accept a Challenge
	var allowChallenge=function(challengeId){
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var url=appLinkFactory.getUrl('allowChallenge');
		var param={
			'userid':userId,
			'challengeid':challengeId
		}
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
			//Success
			$scope.islocked=data.islocked;
			$scope.isplayable=data.isplayable;
			if(data.islocked){
				$scope.completedMessage=data.message==null?"Challenge Completed":data.message;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Comments
	var getChallengeComments=function(challengeId,pageNo){
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url = appLinkFactory.getUrl('commentlist').replace(new RegExp('{id}', 'g'), challengeId) + "?pagenumber=" + $scope.commentsPageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.comments=$scope.comments.concat(data.data);
				$scope.showMoreComments=data.more;
			}
			else{
				$scope.comments=[];
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Claims
	var getChallengeClaims=function(challengeId,page){
		if(page==undefined){
			page=0;
		}
		if(page==0){
			$scope.challengeClaims=[];
		}
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url=appLinkFactory.getUrl('challengeClaims').replace(new RegExp('{id}','g'),gameId)+"?keywords_type=challengeid&keywords="+challengeId+"&pagenumber="+page;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.challengeClaims=$scope.challengeClaims.concat(data.data);
				$scope.showMoreClaims=data.more;
			}
			else{
				$scope.challengeClaims=[];
				$scope.showMoreClaims=false;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//get Index of challenge to highlight
	var getChallengeIndex=function(challenges,challengeId){
		var value;
		challenges.forEach(function(e,i){
			if(e.id == parseInt(challengeId)){
				value=i;
				return false;
			}
		})
		return value;
	}

	//Challenges in Game
	var getChallengesInGame=function(){
		
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url = appLinkFactory.getUrl('topicChallenges').replace(new RegExp('{id}', 'g'), $scope.challenge.quest.id) + "?isvideo=false";
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				var y;
				$scope.games=data.data;
				if(challengeId!=0)
					y=getChallengeIndex($scope.games,challengeId);
				else
					y=0;
				$scope.challenge.nextChallengeId=$scope.games[y+1]!=undefined?$scope.games[y+1].id:undefined;
			}
			else{
				alertify.error(data.error);
			}

		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	var init=function(){
		$scope.comments=[];
		$scope.isIlike=false;
		$scope.response={};
		$scope.quiz_option_id;
		$scope.quiz_option_id_list=[];
		$scope.commentsPageNo=0;
		$scope.claimsPageNo=0;
		$scope.challengeResponse = {};
		$scope.nextbtn = true;
		$scope.prevbtn = true;
		challengeId = $location.search().id;
		var topicid = $location.search().topicid;
		var typevalue = $location.search().type; 		
		if(typevalue=='ilike'){
			ilikeIwish();		
		}
		else{
			if(!challengeId && challengeId==null)
			$location.path('/challenges');
		getGamePoints();
		getChallenge(challengeId);
		getChallengeComments(challengeId, $scope.commentsPageNo);
		getChallengeClaims(challengeId, $scope.claimsPageNo);	

		var url = appLinkFactory.getUrl('topicChallenges').replace(new RegExp('{id}', 'g'), topicid) + "?isvideo=false";

		var req = httpRequestService.connect(url, 'GET');
		req.success(function (data) {
		    $scope.chlglist = data;
		    angular.forEach($scope.chlglist.data, function (val) {
		        if (val.id == challengeId) {
		            if (val.nextChallengeId != 0) {
		                $scope.nextvalId = val.nextChallengeId;
		            }
		            else {
		                $scope.nextbtn = false;
		            }
		            if (val.previousChallengeId != 0) {
		                $scope.prevvalId = val.previousChallengeId;
		            }
		            else {
		                $scope.prevbtn = false;
		            }
		        }
		    })
		})
		req.error(function (err, msg) {

		})
	}
		
	}
	
	$scope.tags = [
		{ text: 'Tag1' },
		{ text: 'Tag2' },
		{ text: 'Tag3' }
	  ];

	var ilikeIwish = function(){

		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
	
		var url=appLinkFactory.getUrl('iLikeIwish').replace(new RegExp('{id}', 'g'), gameId);
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			$scope.challenge=data.data;	
			$scope.nextbtn = false;	
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	   }

	init();

 

   	//Activity Comments
	$scope.getActivityComments=function(index,id,pageNo){
		if(pageNo==undefined){
			pageNo=1;
			if(!$scope.showUserPhotos)
				$scope.challengeClaims[index].pageNo=1;
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('claimComments').replace(new RegExp('{id}','g'),id)+"?session_key="+sessionKey+"&page="+pageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				
					if(!$scope.challengeClaims[index].comments){
						$scope.challengeClaims[index].comments=[];
					}
					else if(pageNo==1){
						$scope.challengeClaims[index].comments=[];
					}
					$scope.challengeClaims[index].comments=$scope.challengeClaims[index].comments.concat(data.data);
					$scope.challengeClaims[index].more=data.more;					
				
			}
			else{
				
					$scope.challengeClaims[index].comments=[];
					$scope.challengeClaims[index].more=false;
				
			}
			
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}


	$scope.closePDF=function(){

		if(!$scope.islocked && $scope.isplayable){
			$scope.claimChallenge();
		}
		else if($scope.islocked || !$scope.isplayable){
			
			
			$scope.challengeConfig.blurText=false;
			$scope.challengeConfig.blurPdf=false;
		}
	}

	$scope.challengecompletemethod = function () {
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var url = appLinkFactory.getUrl('challengeComplete');

	    var param = {

	        "userid":userId,
	        "challengeid":$scope.challenge.id,
	        "activitytype":"APT", 
	        "isaccepted":true, 
	        "createdby":userId
	    };
	  
	    var req = httpRequestService.connect(url, 'POST', param);
	    req.success(function (data) {
	      
	    })
	    req.error(function (err, msg) {
	        //alertify.error('Please Check Internet Connectivity (or) No data is found');
	    });

	}

	$scope.challengecheck = function (chlg) {	 
	        var path1 = $location.$$absUrl;
	        path1 = path1.substr(0, (path1.indexOf('#') + 1));
	        window.location.href = path1 + "/challenge?id=" + $scope.nextvalId + '&topicid=' + chlg.topicid;	    
	}
	$scope.challengeprevcheck = function (chlg) {
	    var path1 = $location.$$absUrl;
	    path1 = path1.substr(0, (path1.indexOf('#') + 1));
	    window.location.href = path1 + "/challenge?id=" + $scope.prevvalId + '&topicid=' + chlg.topicid;
	}


	$scope.uploadFile = function () {
	    //document.getElementById('challengePhoto').click();
	    //var preview = document.getElementById('challengePhoto');

	    var preview = document.getElementById('showimg');

	    var file = preview.files[0];
	    if (file == undefined) {
	        return;
	    }
	    var formData = new FormData();

	    formData.append("fileUpload", file);

	    var url = appLinkFactory.getUrl('FileUpload');

	    var uploadCheck = "UPLOAD";

	    var fileuploadreq = httpRequestService.connect(url, 'POST', formData, uploadCheck);



	    fileuploadreq.success(function (data) {
	        $scope.imageurlcomment = data.message;
	    })
	    fileuploadreq.error(function (err, msg) {
	        console.log('error');
	    })

	}

	//Resize listener for iframe
	// $timeout(function(e) {
	// 	var iframeopen=false;
	// 	var iframewidth=$('iframe').width();
	// 	var iframewindow=$('iframe')[0]!=undefined?$('iframe')[0].contentWindow:"";
	// 	$(iframewindow).on('resize',function(){
	// 		if(iframeopen && $(this).width()<=iframewidth){
	// 			iframeopen=false;
				
	// 			if(!$scope.challenge.claimed&&$scope.challenge.quizOptions&&$scope.challenge.quizOptions.length==1){
	// 				$scope.claimChallenge();
	// 			}
	// 			else if(!$scope.challenge.claimed && ($scope.challenge.quizOptions==undefined || ($scope.challenge.quizOptions!=undefined &&$scope.challenge.quizOptions.length>1))){
					
	// 				$scope.challengeConfig.disableSubmitBtn=false;
	// 				$scope.challengeConfig.blurText=false;
	// 				$scope.challengeConfig.blurPdf=false;
	// 				$scope.$apply();
	// 			}
	// 		}
	// 		else if(!iframeopen && $(this).width()>iframewidth){
	// 			iframeopen=true;
	// 		}
	// 	})
	    // }, 3000);

	
}]);