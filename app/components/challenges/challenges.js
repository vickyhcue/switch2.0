'use strict';

angular.module('switch.challenges', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/challenges', {
    templateUrl: 'app/components/challenges/challenges.html',
    controller: 'challengesController'
  });
}])

.controller('challengesController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {
	$scope.menus=["home","games","watch"];
	$scope.gamesCount=0;
	$scope.showSlider=false;

	$scope.gotoChallenge = function (challenge, game) {
	    //return false;

		var challengePos=[];
		angular.forEach($scope.games,function(e,i){
			challengePos.push($('#carousel'+i+' .owl-item').index($('#carousel'+i).find('.active')[0]));
		});
		challengePos.push($(window).scrollTop());
		appCookieFactory.setValue('switchChallengePosition',JSON.stringify(challengePos));
		if(challenge.title!="Coming Soon")
		    $location.path('/challenge').search('id', challenge.id);
		    $location.path('/challenge').search('topicid', game.id);
	}

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Game Points
	var getGamePoints = function () {
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url = appLinkFactory.getUrl('topicChallengeList').replace(new RegExp('{id}', 'g'), gameId) + "?user_id=" + userid + "&isvideo=false";
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
		    if (data.code == 200) {
		        $scope.topiclist = data.data;
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level = 1;
				$timeout(function () {

				    $('.owl-carousel').owlCarousel({
				        loop: false,
				        margin: 20,
				        dots: false,
				        nav: true,
				        navText: [
                          "<span class='prev-icon'></span>",
                          "<span class='next-icon'></span>"
				        ],
				        autoplay: false,
				        autoplayTimeout: 2000,
				        autoplayHoverPause: true,
				        responsive: {
				            0: {
				                items: 2
				            },
				            600: {
				                items: 3
				            },
				            1000: {
				                items: 5
				            }
				        }
				    });
				    $timeout(function () {
				        $scope.showSlider = true;
				        var challengePos = appCookieFactory.getValue('switchChallengePosition');

				        if (challengePos != undefined) {
				            challengePos = JSON.parse(challengePos);
				            $scope.games.forEach(function (e, i) {
				                $('#carousel' + i).trigger('to.owl.carousel', [challengePos[i], 0, true]);
				            });
				            $(window).scrollTop(challengePos[$scope.games.length]);
				        }
				    }, 100);
				}, 100);
			}
			else{
				alertify.error(data.error);
                if(data.error=="Invalid session key"){
	                appCookieFactory.clearValue('switchUserInfo');
                    appCookieFactory.clearValue('switchUserTeamInfo');
                    $location.path('/login');
                }
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	

	//Games By Category
	var getGames=function(){
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	    var url = appLinkFactory.getUrl('categoryTopics').replace(new RegExp('{id}', 'g'), gameId) + "?isvideo=false&user_id=" + userId + "&page=" + $scope.gamesPageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				
				$scope.games=$scope.games.concat(data.data);
				if(data.more==true){
					getGames(++$scope.gamesPageNo);
				}
				if(data.more==false){
					$scope.games.forEach(function(e,i){
						$scope.games[i].challenges=[];
						$scope.games[i].pageNo=1;
						getChallengesInGame(e.id,i,1);
					})				
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenges in Game
	var getChallengesInGame=function(gameId,x,offset){
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var url = appLinkFactory.getUrl('topicChallenges').replace(new RegExp('{id}', 'g'), gameId) + "?page=" + offset + "&user_id=" + userId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.games[x].challenges=$scope.games[x].challenges.concat(data.data);
				if(data.more){
					getChallengesInGame(gameId,x,++$scope.games[x].pageNo)
				}
				else{
					$scope.gamesCount++;
					if($scope.gamesCount==$scope.games.length){
						$timeout(function(){
						
							$('.owl-carousel').owlCarousel({
					          loop: false,
					          margin: 20,
					          dots: false,
					          nav: true,
					          navText: [
					            "<span class='prev-icon'></span>",
					            "<span class='next-icon'></span>"
					          ],
					          autoplay: false,
					          autoplayTimeout: 2000,
					          autoplayHoverPause: true,
					          responsive: {
					            0: {
					              items: 2
					            },
					            600: {
					              items: 3
					            },
					            1000: {
					              items: 5
					            }
					          }
					        });
					        $timeout(function(){
					       		$scope.showSlider=true;
					       		var challengePos=appCookieFactory.getValue('switchChallengePosition');
					       		
					       		if(challengePos!=undefined){
					       			challengePos=JSON.parse(challengePos);
					       			$scope.games.forEach(function(e,i){
					       				$('#carousel'+i).trigger('to.owl.carousel',[challengePos[i],0,true]);
					       			});
					       			$(window).scrollTop(challengePos[$scope.games.length]);
					       		}
					       	},100);
						},100);
					}
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}

		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
		
	}

	var init=function(){
		
		getGamePoints();
		$scope.gamesPageNo=1;
		$scope.games=[];
		getGames();		
		
	}
	
	init();

}]);