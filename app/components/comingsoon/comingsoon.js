'use strict';

angular.module('switch.comingsoon', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/comingsoon', {
    templateUrl: 'app/components/comingsoon/comingsoon.html',
    controller: 'comingsoonController'
  }).otherwise({
	redirectTo: "/login"
  });
}])

.controller('comingsoonController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,alertify) {
	$scope.menus=["home","games","watch"];
	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Game Points
	var getGamePoints=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Carousel Images
	var getCarouselImages=function(){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('carousel')+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.carousel=data.data;
				$('#carouselImg').carousel({
					interval:3000
				})
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//User Widget
	var getUserWidget=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userWidget').replace(new RegExp('{id}','g'),userId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.team=data.team;
				//Synchronizing Call to find best team
				getBestTeam();
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Best Team
	var getBestTeam=function(){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('bestTeam')+"?session_key="+sessionKey+'&team_id='+5;//$scope.team['id']
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.bestTeam=data['highestRank'];
				$scope.bestTeam.level=1;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Latest Challenge
	var getLatestChallenge=function(){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('latestChallenge')+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.latestChallenge=data.data;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	var init=function(){
		
		getGamePoints();
		getCarouselImages();
		getUserWidget(); //Contains synchronize call to find best team ~~Not Working~~
		getLatestChallenge();
	}
	
	init();


}]);