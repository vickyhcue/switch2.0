'use strict';

angular.module('switch.comments', ['ngRoute','ngAlertify'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/comments', {
    templateUrl: 'app/components/comments/comments.html',
    controller: 'commentsController'
  });
   $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])


.controller('commentsController', ['$scope','$window','$location','$sce','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$window,$location,$sce,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {

	$scope.trustSrc = function(src) {
    	return $sce.trustAsResourceUrl(src);
  	}
	
	var challengeId;

	//Select a claim in challenge
	$scope.selectClaim=function(id,isSingle){
		if(isSingle){
			$scope.quiz_option_id=id;
		}
		else{
			var ind=$scope.quiz_option_id_list.indexOf(id);
			if(ind>=0){
				$scope.quiz_option_id_list.splice(ind,1);
			}
			else{
				$scope.quiz_option_id_list.push(id);
			}
		}
	}

	//trigger photo
	$scope.uploadPhoto=function(){
		document.getElementById('challengePhoto').click();
	}

	// //read photo
	// $scope.$watch('file', function(newfile, oldfile) {
 //      if(angular.equals(newfile, oldfile) ){
 //        return;
 //      }

 //      console.log(newfile);
 //    });

	//Comment a challenge
	$scope.commentChallenge=function(){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengeComment')+"?session_key="+sessionKey+"&secret_key="+CONFIG.apiKey+"&type=challenge&type_id="+challengeId+"&content="+$scope.comment;
		var req=httpRequestService.connect(url,'POST');
		req.success(function(data){
			if(data.code==200){
				$scope.challenge=data;
				if(!$scope.challenge.accepted){
					acceptChallenge(challengeId);
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Load More Comments
	$scope.nextPageComments=function(){
		$scope.commentsPageNo+=1;
		getChallengeComments(challengeId,$scope.commentsPageNo);
	}

	//Claim a challenge
	$scope.claimChallenge=function(){
		var data;
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('claimChallenge');
		switch($scope.challenge.challengeTypeId){
			case 1: 
					if($scope.response==undefined || $scope.response.comment==undefined || $scope.response.comment==""){
						alertify.error('Enter Comments to complete the challenge');
						return false;
					}
					if($scope.challenge.photoOnly && !$scope.response.filepreview){
						alertify.error('Upload an image to complete the challenge');
						return false;
					}
					
					
					url+="?secret_key="+CONFIG.apiKey+"&session_key="+sessionKey+"&challenge_id="+parseInt(challengeId)+"&comment="+$scope.response.comment;
					data={};
					break;
			case 2:
			case 10:
			case 11:
					if($scope.challenge.multiSelect || $scope.challenge.challengeTypeId==2){
						var quiz_options=[];
						$scope.quiz_option_id_list.forEach(function(e,i){
							quiz_options.push({'id':e});
						})
						data={
							challenge_id: parseInt(challengeId), 
							secret_key: CONFIG.apiKey, 
							session_key: sessionKey, 
							quiz_option_id_list:JSON.stringify(quiz_options)
						};
					}
					else{
						data={
							challenge_id: parseInt(challengeId), 
							secret_key: CONFIG.apiKey, 
							session_key: sessionKey, 
							quiz_option_id: $scope.quiz_option_id
						};
					}
					break;
		}
	    var param = "";
	    var header;
	    if($scope.challenge.challengeTypeId==1){
	    	// header={'header':{}}
	    	// param=new FormData();
	    	// param.append('file',$scope.response.file);
	    	// param.append('filename','file_name');
	    	var data = new FormData();
			data.append("file", $scope.response.file);
			data.append("file_name", "sample.png");

			var xhr = new XMLHttpRequest();

			xhr.addEventListener("readystatechange", function () {
			  if (this.readyState === 4 && (this.status==200||this.status==201)) {
			  	console.log(this);
			    data=JSON.parse(this.responseText);
			    if(data.code==200){
					alertify.success((data.completionMessage?data.completionMessage:'Completed Successfully')+' Points Earned:'+data.points);
					$window.location.reload();
				}
				else{
					alertify.error(data.error)
				}
			  }
			  else{
			  	alertify.error(data.error)
			  }
			});

			xhr.open("POST", url);

			xhr.send(data);
	    }
	    else{
		    for (var key in data) {
		         if (data.hasOwnProperty(key)) {
		         		if(data[key]!=undefined)
		               		param+=(encodeURIComponent(key) + "=" + encodeURIComponent(data[key])) + "&";
		         }
		    }
		
	    
			var req=httpRequestService.connect(url,'POST',param);
			req.success(function(data){
				if(data.code==200){
					alertify.success(data.completionMessage?data.completionMessage:'Completed Successfully'+' Points Earned:'+data.points);
					$window.location.reload();
				}
				else{
					alertify.error(data.error)
				}			
			})
			req.error(function(err,msg){
				//alertify.error('Please Check Internet Connectivity (or) No data is found');
			});
		}
	}

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Game Points
	var getGamePoints=function(){
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid)+"?session_key="+sessionkey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Challenge
	var getChallenge=function(challengeId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challenge').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.challenge=data;
				if(!$scope.challenge.accepted){
					acceptChallenge(challengeId);
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Accept a Challenge
	var acceptChallenge=function(challengeId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('acceptChallenge')+"?session_key="+sessionKey+"&type_id="+challengeId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			//Success
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Comments
	var getChallengeComments=function(challengeId,pageNo){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengeComments').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey+"&page="+$scope.commentsPageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.comments=$scope.comments.concat(data.data);
				$scope.showMoreComments=data.more;
			}
			else{
				$scope.comments=[];
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Claims
	var getChallengeClaims=function(challengeId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengeClaims').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.challengeClaims=data.data;
			}
			else{
				$scope.challengeClaims=[];
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	var init=function(){
		$scope.comments=[];
		$scope.response={};
		$scope.quiz_option_id;
		$scope.quiz_option_id_list=[];
		$scope.commentsPageNo=1;
		challengeId=$location.search().id;
		if(!challengeId && challengeId==null)
			$location.path('/challenges');
		getGamePoints();
		getChallenge(challengeId);
		getChallengeComments(challengeId,$scope.commentsPageNo);		
		getChallengeClaims(challengeId);
		
	}
	
	init();
}]);