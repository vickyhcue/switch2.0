﻿'use strict';

angular.module('switch.feedback', ['ngRoute', 'ngAlertify'])

.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/feedback', {
        templateUrl: 'app/components/feedback/feedback.html',
        controller: 'feedbackController'
    });
}])

.controller('feedbackController', ['$scope', '$location', '$window', 'appCookieFactory', 'appLinkFactory', 'httpRequestService', 'CONFIG', '$timeout', 'alertify',
	function ($scope, $location, $window, appCookieFactory, appLinkFactory, httpRequestService, CONFIG, $timeout, alertify) {
	    var count, currentCount;
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');


	    $scope.logout = function () {
	        appCookieFactory.clearValue('switchUserInfo');
	        $location.path('/login');
	    }

	    //select rating
	    $scope.selectRating = function (id) {
	        $scope.rating.selectedId = id;
	    }

	    //select topic
	    $scope.selectTopic = function (value) {
	        $scope.topicvalue = value;
	    }

	    //Submit Feedback
	    $scope.submitFeedback = function () {
	        if ($scope.rating == undefined || $scope.rating.selectedId == undefined) {
	            alertify.error('Please rate your experience of app');
	            return false;
	        }
	        else if ($scope.topic == undefined || $scope.topic.selectedId == undefined) {
	            alertify.error('Please select a topic to give feedback');
	            return false;
	        }
	        else if ($scope.comment == undefined || $scope.comment.commentText == null) {
	            alertify.error('Please enter your comment for the feedback topic selected');
	            return false;
	        }
	        claimChallenge(CONFIG.feedbackRating, $scope.rating.selectedId, 'OPT');
	        claimChallenge(CONFIG.feedbackTopic, $scope.topic.selectedId, 'OPT');
	        claimChallenge(CONFIG.feedbackComment, $scope.comment.commentText, 'TXT');
	    }

	    //Claim a challenge
	    var claimChallenge = function (id, val, type) {
	        var data;
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	        var url = appLinkFactory.getUrl('claimChallenge');
	        data = {
	            challenge_id: parseInt(id),
	            secret_key: CONFIG.apiKey,
	            session_key: sessionkey,
	        };
	        if (type == "OPT") {
	            data.quiz_option_id = val;
	        }
	        else {
	            data.comment = val;
	        }
	        var param = "";
	        for (var key in data) {
	            if (data.hasOwnProperty(key)) {
	                if (data[key] != undefined)
	                    param += (encodeURIComponent(key) + "=" + encodeURIComponent(data[key])) + "&";
	            }
	        }


	        var req = httpRequestService.connect(url, 'POST', param);
	        req.success(function (data) {
	            if (data.code == 200) {
	                currentCount++;
	                if (currentCount == count) {
	                    $timeout(function () { $window.location.reload() }, 3000);
	                    alertify.success('Feedback Submitted Successfully');
	                    currentCount = 0;
	                }
	            }
	            else {
	                if (id == CONFIG.feedbackRating)
	                    alertify.error('Please rate to submit');
	                if (id == CONFIG.feedbackTopic)
	                    alertify.error('Please select a topic to submit');
	            }
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        });
	    };

	    //Game Points
	    var getGamePoints = function () {
	        var userid = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');

	        var url = appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}', 'g'), userid);
	        var req = httpRequestService.connect(url, 'GET');
	        req.success(function (data) {
	            if (data.code == 200) {
	                $scope.username = data.name;
	                $scope.points = data.points;
	                $scope.photo = {};
	                $scope.photo['sm'] = data.photoSmall;
	                $scope.photo['lg'] = data.photoLarge;
	                $scope.level = 1;
	            }
	            else {
	                alertify.error(data.error);
	            }
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        })
	    }

	    //Challenge
	    var getChallenge = function (topic) {
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	        var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');

	        if (topic == "rating") {
	            var url = appLinkFactory.getUrl('feedbackRating').replace(new RegExp('{id}', 'g'), gameId);
	        }
	        else {
	            var url = appLinkFactory.getUrl('feedbacksetting').replace(new RegExp('{id}', 'g'), gameId);
	        }

	        var req = httpRequestService.connect(url, 'GET');
	        req.success(function (data) {
	            switch (topic) {
	                case 'rating': $scope.rating = data; break;
	                case 'topic': $scope.topic = data; break;
	                case 'comment': $scope.comment = data; break;
	            }
	            angular.forEach($scope.rating, function (item) {
	                item.active = false;
	            })
	            angular.forEach($scope.topic, function (item) {
	                item.active = false;
	            })	            
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        });
	    }

	    //Accept a Challenge
	    var acceptChallenge = function (challengeId) {
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	        var url = appLinkFactory.getUrl('acceptChallenge') + "?type_id=" + challengeId;
	        var req = httpRequestService.connect(url, 'GET');
	        req.success(function (data) {
	            //Success
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        });
	    }

		var getfeedback = function () {
			var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
			var param = {
				"bundleid":gameId,
				"pageNumber":0,
				"pageSize":10,
				"feedbacktypeid":"1"
			}


	        var url = appLinkFactory.getUrl('feedbacksettings');
	        var reqsetting = httpRequestService.connect(url, 'POST',param);
	        reqsetting.success(function (data) {
				
				$scope.feedbacklist = data[0];
	        })
	        reqsetting.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        });
	    }

	    var init = function () {
	        $scope.comment = {};
	        $scope.commenttext = "";
	        currentCount = 0;
	        count = 3;
	        $scope.ratingTitle = "Please rate your experience of the app";
	        $scope.feedbackTitle = "Select your feedback topic below";
	        $scope.commentTitle = "Share your feedback";
	        $scope.poorshow = false;
	        $scope.badshow = false;
	        $scope.averageshow = false;
	        $scope.goodshow = false;
	        $scope.awesomeshow = false;
			getGamePoints();
			getfeedback();
	        getChallenge('rating');
	        getChallenge('topic');

	    }

	    init();

	    $scope.feedbackimageclick = function (val) {

	        angular.forEach($scope.rating, function (item) {
	            if (item.id == val.id) {
	                item.active = true;
	            }
	            else {
	                item.active = false;
	            }
	        })

	        $scope.ratingID = val.id;
	    }

	    $scope.submitfeebackdata = function () {

	        if ($scope.ratingID == undefined) {
	            alertify.error('Please rate your experience of app');
	            return false;
	        }
	        else if ($scope.topicvalue == undefined) {
	            alertify.error('Please select a topic to give feedback');
	            return false;
	        }
	        else if ($scope.commenttext == undefined || $scope.commenttext == "") {
	            alertify.error('Please enter your comment for the feedback topic selected');
	            return false;
	        }

	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	        var params = {
	            "userid": userId,
	            "bundleid": gameId,
	            "ratingtypeid": $scope.ratingID,
	            "description": $scope.commenttext,
	            "createdby": userId,
	            "feedbacktopic": $scope.topicvalue
	        }

	        var url = appLinkFactory.getUrl('feedbackAdd');
	        var req = httpRequestService.connect(url, 'POST', params);
	        req.success(function (data) {
	            $scope.commenttext = "";
	            $scope.topicvalue = "";
	            angular.forEach($scope.rating, function (item) {
	                item.active = false;
	            })
	            //$window.location.reload();
	            //alertify.success("Feeback added successfully")
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        })



	        var params = {
	            "userid": userId,
	            "bundleid": gameId,
	            "activitytype": "FBK",
	            "commenttext": $scope.commenttext,
	        }

	        var url = appLinkFactory.getUrl('useractivityAdd');
	        var req = httpRequestService.connect(url, 'POST', params);
	        req.success(function (data) {
	            alertify.success("Feeback added successfully");
	            //$window.location.reload();
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        })
	    }

	}]);