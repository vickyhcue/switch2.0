'use strict';

angular.module('switch.feedback', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/feedback', {
    templateUrl: 'app/components/feedback/feedback.html',
    controller: 'feedbackController'
  });
}])

.controller('feedbackController', ['$scope','$location','$window','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,$window,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {
	var count,currentCount;
	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//select rating
	$scope.selectRating=function(id){
		$scope.rating.selectedId=id;
	}

	//select topic
	$scope.selectTopic=function(id){
		$scope.topic.selectedId=id;
	}

	//Submit Feedback
	$scope.submitFeedback=function(){
		if($scope.rating==undefined||$scope.rating.selectedId==undefined){
			alertify.error('Please rate your experience of app');
			return false;
		}
		else if($scope.topic==undefined||$scope.topic.selectedId==undefined){
			alertify.error('Please select a topic to give feedback');
			return false;
		}
		else if($scope.comment==undefined || $scope.comment.commentText==null){
			alertify.error('Please enter your comment for the feedback topic selected');
			return false;
		}
		claimChallenge(CONFIG.feedbackRating,$scope.rating.selectedId,'OPT');
		claimChallenge(CONFIG.feedbackTopic,$scope.topic.selectedId,'OPT');
		claimChallenge(CONFIG.feedbackComment,$scope.comment.commentText,'TXT');
	}

	//Claim a challenge
	var claimChallenge=function(id,val,type){
		var data;
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('claimChallenge');
		data={
			challenge_id: parseInt(id), 
			secret_key: CONFIG.apiKey, 
			session_key: sessionKey, 
		};
		if(type=="OPT"){
			data.quiz_option_id=val;
		}
		else{
			data.comment=val;
		}
		var param = "";
	    for (var key in data) {
	         if (data.hasOwnProperty(key)) {
	         		if(data[key]!=undefined)
	               		param+=(encodeURIComponent(key) + "=" + encodeURIComponent(data[key])) + "&";
	         }
	    }
		
    
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
			if(data.code==200){
				currentCount++;
				if(currentCount==count){
					$timeout(function(){$window.location.reload()},3000);
					alertify.success('Feedback Submitted Successfully');
					currentCount=0;
				}
			}
			else{
				if(id==CONFIG.feedbackRating)
					alertify.error('Please rate to submit');
				if(id==CONFIG.feedbackTopic)
					alertify.error('Please select a topic to submit');
			}			
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	};

	//Game Points
	var getGamePoints=function(){
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid)+"?session_key="+sessionkey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	
	//Challenge
	var getChallenge=function(challengeId,topic){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challenge').replace(new RegExp('{id}','g'),challengeId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				switch(topic){
					case 'rating': $scope.rating=data;break;
					case 'topic':$scope.topic=data;break;
					case 'comment':$scope.comment=data;break;
				}
				if(!data.accepted){
					acceptChallenge(challengeId);
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Accept a Challenge
	var acceptChallenge=function(challengeId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('acceptChallenge')+"?session_key="+sessionKey+"&type_id="+challengeId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			//Success
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}


	var init=function(){
		$scope.comment={};
		
		currentCount=0;
		count=3;
		getGamePoints();
		getChallenge(CONFIG.feedbackRating,'rating');
		getChallenge(CONFIG.feedbackTopic,'topic');
		getChallenge(CONFIG.feedbackComment,'comment');
		
	}
	
	init();

}]);