'use strict';

angular.module('switch.follow', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/followers', {
    templateUrl: 'app/components/follow/follow.html',
    controller: 'followController'
  });
  $routeProvider.when('/following', {
    templateUrl: 'app/components/follow/follow.html',
    controller: 'followController'
  });
}])

.controller('followController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}
	
	$scope.goToProfile=function(id){
		$location.path('/profile').search('id',id);
	}
	
	//Team User List
	var getUsers=function(searchId,type){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		if(searchId==undefined){
			searchId=userId;
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		if(type=="follower"){
			var url=appLinkFactory.getUrl('follower').replace(new RegExp('{id}','g'),searchId)+"?page="+$scope.teamPage;
		}
		else{
			var url=appLinkFactory.getUrl('following').replace(new RegExp('{id}','g'),searchId)+"?page="+$scope.teamPage;	
		}
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.teamUsers=$scope.teamUsers.concat(data.data);
				//$scope.teamName=data.team.name;
				$scope.teamPage++;
				if(data.more){
					getUsers(searchId,type);
				}
			}
			else{
				alertify.error(data.error);
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	
	

	var init=function(){
		$scope.teamUsers=[];
		$scope.teamPage=1;
		$scope.achievements=[];	
		$scope.otherProfile=true;

		var searchId=$location.search().id;
		$scope.screenName=$location.path().slice(1);
		if($location.path().indexOf('/following')>-1){
			getUsers(searchId,'FOLLOWING');
		}
		else{
			getUsers(searchId,'FOLLOWER');	
		}
		
		
	}
	
	init();

}]);