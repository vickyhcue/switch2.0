'use strict';

angular.module('switch.help', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/help', {
    templateUrl: 'app/components/help/help.html',
    controller: 'helpController'
  });
}])

.controller('helpController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {
	
    $scope.showtour=function(){
      $location.path('/tour').search('page','help');
     }
}]);