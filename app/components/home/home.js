'use strict';

angular.module('switch.home', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/home', {
    templateUrl: 'app/components/home/home.html',
    controller: 'homeController'
  }).otherwise({
	redirectTo: "/login"
  });
}])

.controller('homeController', ['$scope','$location','$window','$timeout','appCookieFactory','appLinkFactory','CONFIG','httpRequestService','alertify',
	function($scope,$location,$window,$timeout,appCookieFactory,appLinkFactory,CONFIG,httpRequestService,alertify) {
	$scope.menus=["home","games","watch"];
	$scope.iLikeChallenge=CONFIG.iLikeChallenge;
	$scope.logout = function () {
	    var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
	    var params = {
	        "userid":userId,
            "devicetype":'WEB',
	        "screentype":'SWITCH'
	    }

	    var url = appLinkFactory.getUrl('logout');
	    var req=httpRequestService.connect(url,'POST',params);
	    req.success(function (data) {
      	appCookieFactory.setValue('switchtourInfo',false);
	        appCookieFactory.clearValue('switchUserInfo');
	        appCookieFactory.clearValue('switchUserTeamInfo');
	        $location.path('/login');
	    });
	    req.error(function (err, msg) {
	    })
		
	}

	$scope.gotoChallengeId=function(id,val){
		if(val==undefined && id!=0){
			val="Challenge";
		}
		if(val=="Video" && id!=0){
			$location.path('/video').search('id',id);
		}
		else if(id!=0){
			$location.path('/challenge').search('id',id);	
		}
		else if(val==undefined && id==0){
			$location.path('/challenge').search('type','ilike');	
		}
	}

	$scope.gotoilikeiwish = function(){
		$location.path('/ilikeiwish');	
	}

	$scope.gotoWeb=function(url){
		if($scope.isUrl(url)){
			$window.open(url, '_blank');
		}
		else if(!isNaN(url)){
			$location.path('/challenge').search('id',parseInt(url));
		}
	}

	//Game Points
	var getGamePoints=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');

		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userId)+"?game_id="+gameId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				data.itemPhotos=[];
				data.mesra=[];
				appCookieFactory.setValue('switchUserTeamInfo',JSON.stringify(data));
				$scope.username=data.name;
				$scope.points = data.points || 0;
				$scope.overallPoints=data.points;
				$scope.maxPoints=data.nextLevelStartPoint;
				$scope.challengeCompleted=data.challengesCompletedNo;
				$scope.challengeTotal=data.challengesTotalNo;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.achievements=data.itemPhotos?data.itemPhotos:[];
				$scope.photo['lg']=data.photoLarge;
				$scope.level=data.level;
				$scope.progress=$scope.maxPoints?window.Math.floor((($scope.overallPoints/$scope.maxPoints)*100)):100;
				$scope.progressCss={
					'width':$scope.progress+'%'
				}
				
				$scope.progressBarRight={
					'animation-name':'loading-'+(($scope.progress>=50)?50:$scope.progress)
				}
				$scope.progressBarLeft={
					'animation-name':'loading-'+(($scope.progress-50)>0?($scope.progress-50):0)
				}
				
			

				$scope.showtourinfo = appCookieFactory.getValue('switchtourInfo');				
		        $scope.userfirstlogin = appCookieFactory.getValue('switchfirstlogin');

				if($scope.userfirstlogin == undefined ||(($scope.showtourinfo == undefined || $scope.showtourinfo == 'false') && $scope.overallPoints == 0 )){
					appCookieFactory.setValue('switchtourInfo',true);
					appCookieFactory.setValue('switchfirstlogin',true);
					$location.path('/tour').search('page','home');
				}

			}
			else{
				alertify.error(data.error);
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
					appCookieFactory.clearValue('switchUserTeamInfo');
					$location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Carousel Images
	var getCarouselImages=function(){
	    var bundleId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var url = appLinkFactory.getUrl('carousellist');
	    var param = {
	        'id': userId,
	        'bundleid': bundleId
	    }
	    var req = httpRequestService.connect(url, 'POST', param);
		req.success(function(data){
			if(data.code==200){
				$scope.carousel=data.data;
				
				$('#carouselImg').carousel({
				    interval: parseInt(data.slidetime) * 1000
				})
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//User Widget
	var getUserWidget=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userWidget').replace(new RegExp('{id}','g'),userId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.team=data.team;
				//Synchronizing Call to find best team
				if(data.team!=undefined && data.team.length>0)
					getBestTeam();
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Best Team
	var getBestTeam=function(){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('bestTeam')+"?session_key="+sessionKey+'&team_id='+$scope.team['id'];
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.bestTeam=data['highestRank'];
				$scope.bestTeam.level=1;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Latest Challenge
	var getLatestChallenge=function(){
	    var bundleId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	    var url = appLinkFactory.getUrl('latestChallenge');
	    var param = {
	        "id": userId,
	        "bundleid": bundleId
	    }
	    var req = httpRequestService.connect(url, 'POST', param);
		req.success(function(data){
			if(data.code==200){
				if(data.All_content_complete=="X"){
					$scope.isComplete=true;
					$scope.completed={title:'Stay Tuned',imageLarge:CONFIG.latestActivityImg,id:0}
				}
				else{
					$scope.isComplete=false;
					$scope.latestChallenge=data.data;
				}
				$timeout(function(){
					
						$('.owl-carousel').owlCarousel({
				          loop: false,
				          margin: 20,
				          dots: false,
				          nav: true,
				          navText: [
				            "<span class='prev-icon'></span>",
				            "<span class='next-icon'></span>"
				          ],
				          autoplay: true,
				          autoplayTimeout: 2000,
				          autoplayHoverPause: true,
				          responsive: {
				            0: {
				              items: 2
				            },
				            600: {
				              items: 3
				            },
				            1000: {
				              items: 3
				            }
				          }
				        });
				        $timeout(function(){
				       		$scope.showSlider=true;
				       	},100);
					},100);
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//URL Checker
	$scope.isUrl=function(s) {
	   var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
	   return regexp.test(s);
	}

	var init=function(){

		$scope.showSlider=false;
		$scope.achievements=[];
		getGamePoints();
		getCarouselImages();
		//getUserWidget(); //Contains synchronize call to find best team ~~Not Working~~
		getLatestChallenge();
	}
	
	init();


}]);