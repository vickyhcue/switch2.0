'use strict';

angular.module('switch.ilikeiwish', ['ngRoute','ngAlertify','ngSanitize'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/ilikeiwish', {
    templateUrl: 'app/components/ilikeiwish/ilikeiwish.html',
    controller: 'ilikeiwishController'
  });
   $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])


.controller('ilikeiwishController', ['$scope','$window','$location','$sce','$timeout','$interval','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$window,$location,$sce,$timeout,$interval,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {

	$scope.gotoProfile=function(id){
		$location.path('/profile').search('id',id);
	}

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Load More Claims
	$scope.nextPageClaims=function(){
		$scope.claimsPageNo+=1;
		listcomments($scope.claimsPageNo);	
	}

	//Like a challenge
	$scope.postLike=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var url=appLinkFactory.getUrl('challengeActivity');
		var param={
			"userid": userId,
			"challengeid":$scope.challenge.id,
			"activitytype": "LIK",
			"createdby":userId,
			"active":$scope.challenge.ratedLike==false?true:false
		}
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
				if(data.liked == true){
				$scope.challenge.likeno=parseInt($scope.challenge.likeno)+1;
				}
				else{
				$scope.challenge.likeno=parseInt($scope.challenge.likeno)-1;
				}
				$scope.challenge.ratedLike=data.liked;
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	$scope.submitdata = function(){
		var customcollection = [];
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		$scope.mandatoryfield = false;
		

		angular.forEach($scope.textboxcoll,function(item){
			if(item.Required == 'Y' && item.Value == ''){
			  $scope.mandatoryfield = true;
			}
			customcollection.push(item);
		})
		angular.forEach($scope.dropdowncoll,function(item){
			if(item.Required == 'Y' && item.Value == ''){
				$scope.mandatoryfield = true;
			}
			customcollection.push(item);
		})

		if($scope.mandatoryfield){
			alertify.error("Enter mandatory fields");
			return;
		}

		var params = {
			"userid": userId,
			"bundleid": gameId,
			"activitytype": "ILW",
			"ilikecomment": $scope.ilike,
			"iwishcomment": $scope.iwish,
			"customfieldinfo": {
				'data':customcollection
			}
		}
	
		var url = appLinkFactory.getUrl('useractivityAdd');
		var submitreq = httpRequestService.connect(url, 'POST', params);
		submitreq.success(function (data) {	
			$scope.ilike = "";
			$scope.iwish = "";
			angular.forEach($scope.textboxcoll,function(item){
				item.Value = "";
			})
			angular.forEach($scope.dropdowncoll,function(item){
				item.Value = "";
			})
			alertify.success("submitted successfully");
		})
		submitreq.error(function (err, msg) {
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}


	var init=function(){
			$scope.ilike = "";
			$scope.iwish = "";
			$scope.claimsPageNo = 0;
			ilikeIwish();	
			listcomments($scope.claimsPageNo);	
	}
	
	var ilikeIwish = function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
	
		var url=appLinkFactory.getUrl('iLikeIwish').replace(new RegExp('{id}', 'g'), gameId) + '?user_id' + userId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			$scope.challenge=data.data;	
			$scope.textboxcoll = [];
			$scope.dropdowncoll = [];
			angular.forEach($scope.challenge.customcontrolsinfo.data, function (val) {
				if(val.Type == 'text'){
					$scope.textboxcoll.push(val);
				}
				else{
					$scope.dropdowncoll.push(val);
				}
			});
		})
		req.error(function(err,msg){
		})
	   }

	var listcomments = function(pageno){
		if(pageno==0){
			$scope.challengeClaims=[];
		}
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId') + '?pagenumber=' + pageno;
	
		var url=appLinkFactory.getUrl('listcomments').replace(new RegExp('{id}', 'g'), gameId);
		var listreq=httpRequestService.connect(url,'GET');
		listreq.success(function(data){
			$scope.challengeClaims=$scope.challengeClaims.concat(data.data);
			$scope.showMoreClaims=data.more;
		})
		listreq.error(function(err,msg){
		})
	   }



	init();
	
}]);