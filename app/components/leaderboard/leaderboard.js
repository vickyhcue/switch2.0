'use strict';

angular.module('switch.leaderboard', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/leaderboard', {
    templateUrl: 'app/components/leaderboard/leaderboard.html',
    controller: 'leaderboardController'
  });
}])

.controller('leaderboardController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {
	$scope.menus=["home","games","watch"];
	$scope.gamesCount=0;
	$scope.showSlider=false;
	
	function removeDuplicates(data){
		var prevdata={};
		var tempdata=[];
		data.forEach(function(e,i){
			if(prevdata.points!=undefined && prevdata.points==e.points){
				if(prevdata.subinfo!=undefined){
					prevdata.subinfo.push(angular.copy(e));
				}
				else{
					prevdata.subinfo=[];
					prevdata.subinfo=prevdata.subinfo.concat(angular.copy(e));
				}
			}
			else{
				
				if(prevdata.points!=undefined)
					tempdata.push(angular.copy(prevdata));
				prevdata=angular.copy(e);
			}
		});
		if(prevdata.id!=undefined){
			tempdata.push(prevdata)
		}
		return tempdata;
	}

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Go To User Profile Page
	$scope.gotoProfile=function(id){
		$location.path('/profile').search('id',id);
	}

	//Go To Team Page
	$scope.gotoTeam=function(id){
		$location.path('/team').search('id',id);
	}

	//Load More Individual
	$scope.loadMoreData=function(val,type){

	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
		var url = "";
		
		var param = {
	        'id': userId,
	        'bundleid': gameId,
	        'pagesize': 10
		}
		
		if(type=="search" && val=='individual'){
			$scope.individualboard.search=true;
			$scope.individualboard.page=0;
		}
		if(type=="search" && val=='team'){
			$scope.teamboard.search=true;
			$scope.teamboard.page=0;
		}
		if(!$scope.individualboard.search && !$scope.teamboard.search){
			if(val=='individual'){
				url = appLinkFactory.getUrl('leaderboardPage');
				param.pagenumber = $scope.individualboard.page;
			}
			else{
				url = appLinkFactory.getUrl('leaderboardTeamPage');
				param.pagenumber = $scope.teamboard.page;
			}
		}
		else if($scope.individualboard.search || $scope.teamboard.search){
			if(val=='individual'){
				url = appLinkFactory.getUrl('leaderboardPage');
				param.keywords = $scope.individualboard.keywords;
				param.pagenumber = $scope.individualboard.page;
			}
			else{
				url = appLinkFactory.getUrl('leaderboardTeamPage');
				param.keywords = $scope.teamboard.keywords;
				param.pagenumber = $scope.teamboard.page;
			}
		}

	    var req = httpRequestService.connect(url, 'POST', param);
		req.success(function(data){
			if(data.code==200){
				if(val=='individual'){
				if($scope.individualboard.page==0)
					$scope.individual={'userBoard':[],'mainBoard':[]}
					var dupremoved= data.data;//removeDuplicates(data.data);
					data.data=angular.copy(dupremoved);
					$scope.individual.userBoard=$scope.individual.userBoard.concat(data.data);
					$scope.individual.more=data.more;
					$scope.individualboard.show=true;
					$scope.individualboard.page+=1;
				}
				else{
				if($scope.teamboard.page==0)
					$scope.team={'teamBoard':[],'mainBoard':[]}
					var dupremoved= data.data;//removeDuplicates(data.data);
					data.data=angular.copy(dupremoved);
					$scope.team.teamBoard=$scope.team.teamBoard.concat(data.data);
					$scope.team.more=data.more;
					$scope.teamboard.show=true;
					$scope.teamboard.page+=1;
				}
			}
			else{
				//alertify.error(data.error);
				if(val=="individual"){
					$scope.individual={'userBoard':[],'mainBoard':[]}
				}
				else{
					$scope.team={'teamBoard':[],'mainBoard':[]}
				}
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	
	//Reset Individual
	$scope.resetIndividual=function(){
		$scope.individualboard={};
		$scope.individual={'userBoard':[],'mainBoard':[]};
		$scope.individualboard.page=0;
		$scope.individualboard.search=false;
		$scope.individualboard.keywords="";
		$scope.individualboard.show=false;
		getLeaderBoard($scope.userId,"user");
	}

	//Reset Individual
	$scope.resetTeam=function(){
		$scope.teamboard={};
		$scope.team={'teamBoard':[],'mainBoard':[]};
		$scope.teamboard.page=0;
		$scope.teamboard.show=false;
		$scope.teamboard.search=false;
		$scope.teamboard.keywords="";
		getLeaderBoard($scope.userTeam.id,"team");
	}

	//Load Sub User List
	$scope.loadSubUserList=function(item,mode){
		item.mode=mode;
		$scope.subUserList=item;
	}

	//Game Points
	var getGamePoints=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		
		var url = appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}', 'g'), userId) + "?game_id=" + gameId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
				$scope.userTeam=data.team;
				getLeaderBoard($scope.userTeam.id,"team");	

			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	

	//Games By Category
	var getLeaderBoard = function (id, val) {
	    var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url="";
		if (val == "user") {
		    url = appLinkFactory.getUrl('leaderboard');
		}
		if (val == "team") {
		    url = appLinkFactory.getUrl('leaderboardTeam');
		}
		var param = {
		    "id": userId,
		    "bundleid": gameId
		}
		var req = httpRequestService.connect(url, 'POST', param);
		req.success(function(data){
			if(data.code==200){
				if(val=="user"){
					$scope.individual=data;

				}
				if(val=="team"){
					$scope.team=data;
				}
			}
			else{
				alertify.error(data.error);
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	var init=function(){
		$scope.individualboard={};
		$scope.individualboard.page=0;
		$scope.individualboard.show=false;
		$scope.individualboard.search=false;
		$scope.individualboard.keywords="";
		$scope.teamboard={};
		$scope.teamboard.page=0;
		$scope.teamboard.show=false;
		$scope.teamboard.search=false;
		$scope.teamboard.keywords="";
		$scope.userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		//$scope.userTeam=appCookieFactory.getJsonValue('switchUserTeamInfo','team');
		getGamePoints();
		getLeaderBoard($scope.userId,"user");
		//if($scope.userTeam){
			//getLeaderBoard($scope.userTeam.id,"team");		
		//}
		
	}
	
	init();

}]);