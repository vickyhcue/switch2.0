'use strict';

angular.module('switch.login', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'app/components/login/login.html',
    controller: 'loginController'
  });
}])

.controller('loginController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {
	$scope.login={};
	$scope.login.password="";
	$scope.verifyLogin=function(){	

		if($scope.login.email == undefined || $scope.login.password == undefined){
			alertify.error('Enter valid username & password');
			return false;
		}
		
		//Login Validation
	    var url = appLinkFactory.getUrl('login');
	    var param = {
	        api_key: CONFIG.apiKey,
	        email: $scope.login.email,
	        password: $scope.login.password,
	        devicetype: "WEB",
	        screentype: "SWITCH"
	    }
	    var loginreq = httpRequestService.connect(url, 'POST', param);
		loginreq.success(function(data){
			if(data.code==200){
				appCookieFactory.setValue('switchUserInfo',JSON.stringify(data));
				$location.path('/home');
			}
			else{
			    alertify.error(data.message);
                if(data.error=="Invalid session key"){
	                appCookieFactory.clearValue('switchUserInfo');
                    appCookieFactory.clearValue('switchUserTeamInfo');
                    $location.path('/login');
                }
			}
		})
		loginreq.error(function(err,msg){
			if(err.code == 422){
				alertify.error('Invalid login credentials');
			};
			if(err.code == 423){
				alertify.error('You are not assigned to a project yet, please contact the administrator team for assistance');
			};
		})
		return false;
	}

	$scope.clearusrname = function () {
	    $scope.login.email = '';
	}
	$scope.clearpwd = function () {
	    $scope.login.password = '';
	}

	$scope.openpopup = function () {
	    $scope.showfrgpwd = true;
	    $scope.errormail = false;
	    $scope.emailid = "";
	    $scope.newpassword = "";
	    $scope.cnfmnewpassword = "";
	    $scope.val = {
	        shownewpasswordpop: false
	    }
	}

	$scope.closeemailpop = function () {
	    $scope.showfrgpwd = false;
	    $(".modal-backdrop").hide();
	}

	$scope.submitemail = function () {


	    var url = appLinkFactory.getUrl('forgotPassword');
	    var param = {
	        "username": $scope.emailid
	    }
	    var forgotpwdreq = httpRequestService.connect(url, 'POST', param);
	    forgotpwdreq.success(function (data) {
	        if (data.message == "User Exist") {
	            $scope.showfrgpwd = false;
	            $scope.errormail = false;
	            $scope.userloginID = data.userloginid;
	            $scope.showoptpop = true;
	            $scope.erroropt = false;
	            $(".modal-backdrop").hide();

	        }
	        else {
	            $scope.errormail = true;
	            //alertify.error("Enter a registered email id")
	        }
	    })
	    forgotpwdreq.error(function (err, msg) {
	    })
	}

	$scope.cnfrmotp = function () {
	    var url = appLinkFactory.getUrl('verifyOtp');
	    var param = {
	        "userloginid": $scope.userloginID,
	        "otpnumber": $scope.Otpval
	    }
	    var optreq = httpRequestService.connect(url, 'POST', param);
	    optreq.success(function (data) {
	        if (data.message == "OTP Verified") {
	            $scope.showoptpop = false;
	            $scope.shownewpasswordpop = true;
	            document.getElementById('confirmation').click();
	        }
	        else {
	            $scope.erroropt = true;

	            //alertify.error("Enter a valid OTP")
	        }
	    })
	    optreq.error(function (err, msg) {
	    })
	}

	$scope.changepasswrd = function () {

	    if ($scope.newpassword != $scope.cnfmnewpassword) {
	        $scope.pwderror = true;
	        return;
	    }
	    $scope.pwderror = false;
	    if ($scope.cnfmnewpassword.length < 8) {
	        $scope.pwdlengtherror = true;
	        return
	    }
	    $scope.pwdlengtherror = false;

	    var url = appLinkFactory.getUrl('resetpwd');
	    var param = {
	        "userloginid": $scope.userloginID,
	        "password": $scope.cnfmnewpassword
	    }
	    var chgpwdreq = httpRequestService.connect(url, 'POST', param);
	    chgpwdreq.success(function (data) {
	        if (data.message == "User password updated") {
	            $scope.shownewpasswordpop = false;
	            $(".modal-backdrop").hide();
	            alertify.success("Password updated successfully");
	        }
	        else {

	        }
	    })
	    chgpwdreq.error(function (err, msg) {
	    })
	}

	$scope.opensignup = function () {
	    $location.path('/register');
	}
}]);