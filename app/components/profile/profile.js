'use strict';

angular.module('switch.profile', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/profile', {
    templateUrl: 'app/components/profile/profile.html',
    controller: 'profileController'
  });
}])

.controller('profileController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	$scope.goToTeam=function(id){
		$location.path('/team').search('id',id);
	}
	
	$scope.goToProfile=function(id){
		$location.path('/profile').search('id',id);
	}
	$scope.goToFollowers=function(){

		if($scope.otherProfile && $scope.follower!=0){
			$location.path('/followers').search('id',$location.search().id);
		}
		else if($scope.follower!=0){
			$location.path('/followers');
		}
	}
	$scope.goToFollowing=function(){
		if($scope.otherProfile && $scope.following!=0){
			$location.path('/following').search('id',$location.search().id);
		}
		else if($scope.following!=0){
			$location.path('/following');
		}
	}
	$scope.followUser=function(activeval){
		var id=$location.search().id;
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		
		var url = appLinkFactory.getUrl('userfollow');
		var params = {
		    "userid" : id,
		    "followinguserid":userId,
		    "createdby":userId,
		    "active": activeval
		}
		var req = httpRequestService.connect(url, 'POST', params);
		req.success(function(data){
			if(data.code==200){
				//alertify.success((data.followed?"Following":"Unfollowed")+" successfully")
				$scope.followText=data.followed?"Following":"Follow";
				location.reload();
			}
			else{
				alertify.error(data.error);
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Game Points
	var getGamePoints=function(){
		var	userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var	gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');		
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');

		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userId)+"?game_id="+gameId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points = data.points;
				$scope.overallPoints=data.points;
				$scope.maxPoints=data.nextLevelStartPoint;
				$scope.challengeCompleted=data.challengesCompletedNo;
				$scope.challengeTotal=data.challengesTotalNo;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=data.level;
				$scope.mesra=data.meta?data.meta[0].value:'';
				$scope.follower=data.followerNo;
				$scope.following=data.followingNo;
				$scope.achievements=data.itemPhotos?data.itemPhotos:[];
				$scope.progress=$scope.maxPoints?window.Math.floor((($scope.overallPoints/$scope.maxPoints)*100)):100;
				$scope.progressCss={
					'width':$scope.progress+'%'
				}
				$scope.completion=window.Math.floor($scope.challengeCompleted/$scope.challengeTotal*100);
				$scope.completionCss={
					'width':$scope.completion+'%'
				}
				if(data.team.length != 0){
					$scope.teamName=data.team[0].name;
					$scope.teamId=data.team[0].id;
					//getTeamUsers(data.team.id);
				}
				if($scope.follower!=0){
					getFollowerList(userId);
				}
				if($scope.following!=0){
					getFollowingList(userId);
				}
				$scope.progressBarRight={
					'animation-name':'loading-'+(($scope.progress>=50)?50:$scope.progress)
				}
				$scope.progressBarLeft={
					'animation-name':'loading-'+(($scope.progress-50)>0?($scope.progress-50):0)
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Team User List
	var getTeamUsers=function(teamId){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		
		var url=appLinkFactory.getUrl('teamUsers').replace(new RegExp('{id}','g'),teamId);
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.teamUsers=data.data
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Follower List
	var getFollowerList=function(searchId,type){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		if(searchId==undefined){
			searchId=userId;
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var url=appLinkFactory.getUrl('follower').replace(new RegExp('{id}','g'),searchId);
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.followerList=data.data;
				$scope.followerListMore=data.data.length>5;
				//$scope.teamName=data.team.name;
				//$scope.teamPage++;
				
			}
			else{
				alertify.error(data.error);
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Following List
	var getFollowingList=function(searchId,type){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		if(searchId==undefined){
			searchId=userId;
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var url=appLinkFactory.getUrl('following').replace(new RegExp('{id}','g'),searchId);	
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.followingList =data.data;
				$scope.followingListMore=data.data.length>5;
				
				
				
			}
			else{
				alertify.error(data.error);
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Other User Profile
	var getOtherUserProfile=function(userId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var	gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
        var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
        var url = appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}', 'g'), userId) + "?game_id=" + gameId + '&user_id=' + userid;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
			    $scope.username = data.name;
			    $scope.points = data.points;
			    $scope.overallPoints = data.points;
			    $scope.maxPoints = data.nextLevelStartPoint;
			    $scope.challengeCompleted = data.challengesCompletedNo;
			    $scope.challengeTotal = data.challengesTotalNo;
			    $scope.photo = {};
			    $scope.photo['sm'] = data.photoSmall;
			    $scope.photo['lg'] = data.photoLarge;
			    $scope.level = data.level;
			    $scope.mesra = data.meta ? data.meta[0].value : '';
			    $scope.follower = data.followerNo;
			    $scope.following = data.followingNo;
			    $scope.achievements = data.itemPhotos ? data.itemPhotos : [];
			    $scope.followText = data.followed ? "Following" : "Follow";
			    $scope.progress = $scope.maxPoints ? window.Math.floor((($scope.overallPoints / $scope.maxPoints) * 100)) : 100;
			    $scope.progressCss = {
			        'width': $scope.progress + '%'
			    }
			    $scope.completion = window.Math.floor($scope.challengeCompleted / $scope.challengeTotal * 100);
			    $scope.completionCss = {
			        'width': $scope.completion + '%'
			    }
			    if (data.team) {
			        $scope.teamName = data.team.name;
			        $scope.teamId = data.team.id;
			        //getTeamUsers(data.team.id);
			    }
			    if ($scope.follower != 0) {
			        getFollowerList(userId);
			    }
			    if ($scope.following != 0) {
			        getFollowingList(userId);
			    }
			    $scope.progressBarRight = {
			        'animation-name': 'loading-' + (($scope.progress >= 50) ? 50 : $scope.progress)
			    }
			    $scope.progressBarLeft = {
			        'animation-name': 'loading-' + (($scope.progress - 50) > 0 ? ($scope.progress - 50) : 0)
			    }

			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	

	var init=function(){

		$scope.achievements=[];	
		$scope.otherProfile=false;
		var profileId=$location.search().id;
		if(profileId!=undefined && profileId!=null && profileId!=appCookieFactory.getJsonValue('switchUserInfo','userId')){
			$scope.otherProfile=true;
		}
		if($scope.otherProfile){
			getOtherUserProfile(profileId);
		}
		else{
			getGamePoints();
		}
		
	}
	
	init();

}]);