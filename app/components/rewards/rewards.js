'use strict';

angular.module('switch.rewards', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/rewards', {
    templateUrl: 'app/components/rewards/rewards.html',
    controller: 'rewardsController'
  });

}])

.controller('rewardsController', ['$scope','$location','$sce','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$location,$sce,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {
	
	$scope.trustSrc = function(src) {
    	return $sce.trustAsResourceUrl(src);
  	}

	$scope.menus=["home","games","watch"];
	var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
	var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
	$scope.switchUrl=CONFIG.switchUrl+"?session_key="+sessionKey+"&userId="+userId;
	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}

	//Toggle Redeem
	$scope.toggleRedeem=function(){
		$scope.showRedeem=!$scope.showRedeem;
	}

	//Switch to reward store
	$scope.rewardStore=function(){
		$scope.showRewardStore=!$scope.showRewardStore;
	}

	$scope.refresh=function(){
  		$window.location.reload()
  	}

	//Claim Mesra
	$scope.claimMesra=function(id){
		var secretKey=CONFIG.apiKey;
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('claimMesra')+"?session_key="+sessionKey+"&secret_key="+secretKey+"&type_id="+id;
		var req=httpRequestService.connect(url,'POST');
		req.success(function(data){
			if(data.code==200){
				alertify.success('Claimed Successfully');
				$scope.points=data.pointsAvailableInBundle;
				document.getElementById('confirmation').click();
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Load More
	$scope.loadMore=function(page){
		if(page=="Redeem"){
			getRewards(++$scope.redeemPage);
		}
		else{
			redeemedPoints(++$scope.redeemedPage);
		}
	}


	//$scope.redeemedPoints(++$scope.redeemedPage);

	//Redeemed Points
	var redeemedPoints=function(page){
		if(page==undefined){
			page=1;
		}
		if(page==1){
			$scope.redeemed=[];
		}
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('purchasedRewards').replace(new RegExp('{id}','g'),userId)+"?category_id=1&session_key="+sessionKey+"&page="+page;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.redeemed=$scope.redeemed.concat(data.data);
				$scope.showMoreRedeemed=data.more;
			}
			else{
				$scope.redeemed=[];
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Game Points
	var getGamePoints=function(){
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid)+"?session_key="+sessionkey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				//$scope.points=data.points;
				$scope.mesra=data.meta?data.meta[0].value:"";
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	
	//Challenges in Game
	var getRewards=function(page){
		if(page==undefined){
			page=1;
		}
		if(page==1){
			$scope.rewards=[];
		}
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('rewards')+"?session_key="+sessionKey+"&category_id=1&page="+page;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.points=data.pointsAvailableInBundle;
				$scope.rewards=$scope.rewards.concat(data.data);
				$scope.showMoreRedeem=data.more;
			}
			else{
				$scope.rewards=[];
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	var init=function(){
		$scope.redeemedPage=1;
		$scope.redeemPage=1;
		if(document.documentMode || /Edge/.test(navigator.userAgent)){
			document.getElementById('confirmation').click();
			$scope.isie=true;
			$scope.showRedeem=false;
			$scope.showRewardStore=true;
		}
		else{
			$scope.isie=false;
			$scope.showRedeem=true;
			$scope.showRewardStore=false;	
		}
		
		getGamePoints();
		getRewards($scope.redeemPage);
		redeemedPoints($scope.redeemedPage);				
	}
	
	init();
}]);