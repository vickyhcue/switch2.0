﻿'use strict';

angular.module('switch.settings', ['ngRoute', 'ngAlertify'])

.config(['$routeProvider', '$sceDelegateProvider', function ($routeProvider, $sceDelegateProvider) {
    $routeProvider.when('/settings', {
        templateUrl: 'app/components/settings/settings.html',
        controller: 'settingsController'
    });
    $sceDelegateProvider.resourceUrlWhitelist([
     // Allow same origin resource loads.
     'self',
     // Allow loading from our assets domain. **.
     'https://switch.petronas.com/**'
    ]);
}])


.controller('settingsController', ['$scope', '$window', '$location', '$sce', '$timeout', 'appCookieFactory', 'appLinkFactory', 'httpRequestService', 'CONFIG', 'alertify',
	function ($scope, $window, $location, $sce, $timeout, appCookieFactory, appLinkFactory, httpRequestService, CONFIG, alertify) {

	    $scope.user = {};
	    $scope.response = {};
	    $scope.logout = function () {
			appCookieFactory.clearValue('switchtourInfo');
	        appCookieFactory.clearValue('switchUserInfo');
	        appCookieFactory.clearValue('switchUserTeamInfo');
	        $location.path('/login');
	    }

	    $scope.uploadFile = function () {
	        //document.getElementById('challengePhoto').click();
	        //var preview = document.getElementById('challengePhoto');

	        var preview = document.getElementById('showimg');

	        var file = preview.files[0];
	        if (file == undefined) {
	            return;
	        }
	        var formData = new FormData();

	        formData.append("fileUpload", file);

	        var url = appLinkFactory.getUrl('FileUpload');

	        var uploadCheck = "UPLOAD";

	        var fileuploadreq = httpRequestService.connect(url, 'POST', formData, uploadCheck);



	        fileuploadreq.success(function (data) {
	            $scope.uploadimgEnable = true;
	            $scope.imageurl = data.message.Location;
	        })
	        fileuploadreq.error(function (err, msg) {
	            console.log('error');
	        })

	    }

	    $scope.uploadAvatar = function () {
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	        var avatarUrl = encodeURIComponent($scope.avatarPhoto);

	        var url = appLinkFactory.getUrl('updateUserProfile') + "?photo_url=" + avatarUrl;
	        var req = httpRequestService.connect(url, 'POST');
	        req.success(function (data) {
	            if (data.code == 200) {
	                alertify.success('Updated Profile Successfully');
	                $timeout(function () { $window.location.reload() }, 3000)
	            }
	            else {
	                alertify.error(data.error);
	            }
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        })
	    }

	    //Update Profile
	    $scope.update = function (val) {


	        var secretKey = CONFIG.apiKey;
	        var userId = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');

	        if ($scope.user.name != undefined && $scope.user.name.length <= 1) {
	            alertify.error('Enter Valid Name');
	            return false;
	        }

	        $scope.updatepassword = function () {
	            if (($scope.user.newPassword != undefined && $scope.user.newPassword != null) || ($scope.user.confirmPassword != undefined && $scope.user.confirmPassword != null) || ($scope.user.oldPassword != undefined && $scope.user.oldPassword != null)) {
	                if ($scope.user.oldPassword == null) {
	                    alertify.error('Enter current Password');
	                    return false;
	                }
	                else if ($scope.user.newPassword == undefined) {
	                    alertify.error('Enter a valid password');
	                    return false;
	                }
	                else if ($scope.user.newPassword == null) {
	                    alertify.error('Enter New Password');
	                    return false;
	                }
	                    //else if($scope.user.confirmPassword==null){
	                    //  alertify.error('Enter Confirm password');
	                    //  return false;
	                    //}
	                else if ($scope.user.newPassword != $scope.user.confirmPassword) {
	                    alertify.error('New Password and Confirm Password do not match');
	                    return false;
	                }

	                var url = appLinkFactory.getUrl('updatePassword');
	                var param = {
	                    "newpassword": $scope.user.newPassword,
	                    "oldpassword": $scope.user.oldPassword,
	                    "userid": userId
	                };
	                var req = httpRequestService.connect(url, 'POST', param);
	                req.success(function (data) {
	                    if (data.message == "success") {
	                        alertify.success('Updated successfully');
	                        $scope.user.newPassword = null;
	                        $scope.user.oldPassword = null;
	                        $scope.user.confirmPassword = null;
	                    }
	                    else {
	                        alertify.error(data.message);
	                    }
	                })
	                req.error(function (err) {
	                    //alertify.error('Please Check Internet Connectivity (or) No data is found');
	                })
	            }
	        }

	        var params = {
	            "id": userId,
	            "firstname": $scope.user.name,
	            "userphoto": ($scope.uploadimgEnable) ? $scope.imageurl : $scope.photo['sm'],
	            "updatedby": userId
	        }

	        var url = appLinkFactory.getUrl('profileimageupload').replace(new RegExp('{id}', 'g'), userId) + "?secret_key=" + secretKey;

	        var reqfile = httpRequestService.connect(url, 'PUT', params);
	        reqfile.success(function (data) {
	            if (data.message == "User updated") {
	                //alertify.success('Profile details updated successfully');
	                if (($scope.user.newPassword != undefined && $scope.user.newPassword != null) || ($scope.user.confirmPassword != undefined && $scope.user.confirmPassword != null) || ($scope.user.oldPassword != undefined && $scope.user.oldPassword != null)) {
	                    $scope.updatepassword();
	                }
	                else {
	                    alertify.success('Updated successfully');
	                }
	            }
	            else {
	                alertify.error(data.message);
	            }
	        })
	        reqfile.error(function (err) {

	        })


	    }


	    //Game Points
	    var getGamePoints = function () {
	        var userid = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	        var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
	        var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');

	        var url = appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}', 'g'), userid) + "?game_id=" + gameId;
	        var req = httpRequestService.connect(url, 'GET');
	        req.success(function (data) {
	            if (data.code == 200) {
	                $scope.username = data.name;
	                //$scope.points=data.points;
	                $scope.mesra = data.meta ? data.meta[0].value : "";
	                $scope.user.name = $scope.username;
	                $scope.user.mesra = $scope.mesra;
	                $scope.avatarPhoto = data.avatarPhoto || "";
	                $scope.photo = {};
	                $scope.photo['sm'] = data.photoSmall;
	                $scope.photo['lg'] = data.photoLarge;
	                if (data.photoLarge == data.avatarPhoto) {
	                    $scope.avatarPhoto = "";
	                }
	                $scope.level = 1;
	                $scope.overallPoints = data.points;
	                $scope.maxPoints = data.nextLevelStartPoint;
	                $scope.progress = $scope.maxPoints ? window.Math.floor((($scope.overallPoints / $scope.maxPoints) * 100)) : 100;
	                $scope.progressCss = {
	                    'width': $scope.progress + '%'
	                }

	                $scope.progressBarRight = {
	                    'animation-name': 'loading-' + (($scope.progress >= 50) ? 50 : $scope.progress)
	                }
	                $scope.progressBarLeft = {
	                    'animation-name': 'loading-' + (($scope.progress - 50) > 0 ? ($scope.progress - 50) : 0)
	                }
	            }
	            else {
	                alertify.error(data.error);
	            }
	        })
	        req.error(function (err, msg) {
	            //alertify.error('Please Check Internet Connectivity (or) No data is found');
	        })
	    }

	    var init = function () {
	        $scope.user = {};
	        $scope.avatarPhoto = "";
	        $scope.uploadimgEnable = false;
	        getGamePoints();
	    }

	    init();

	}]);