'use strict';

angular.module('switch.settings', ['ngRoute','ngAlertify'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/settings', {
    templateUrl: 'app/components/settings/settings.html',
    controller: 'settingsController'
  });
   $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])


.controller('settingsController', ['$scope','$window','$location','$sce','$timeout','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','alertify',
	function($scope,$window,$location,$sce,$timeout,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,alertify) {

  $scope.user={};
  $scope.response={};
  $scope.logout=function(){
    appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
  }
  
  $scope.uploadPhoto=function(){
    document.getElementById('challengePhoto').click();
  }

  $scope.uploadAvatar=function(){
    var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
    var avatarUrl=encodeURIComponent($scope.avatarPhoto);
    
    var url=appLinkFactory.getUrl('updateUserProfile')+"?session_key="+sessionkey+"&photo_url="+avatarUrl;
    var req=httpRequestService.connect(url,'POST');
    req.success(function(data){
      if(data.code==200){
        alertify.success('Updated Profile Successfully');
        $timeout(function(){$window.location.reload()},3000)
      }
      else{
        alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
      }
    })
    req.error(function(err,msg){
      //alertify.error('Please Check Internet Connectivity (or) No data is found');
    })
  }

  //Update Profile
  $scope.update=function(){
  
  
    var secretKey=CONFIG.apiKey;
    var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
    if(($scope.user.name!=undefined && $scope.user.name!=null && $scope.user.name.length>=2) || ($scope.user.mesra!=undefined && $scope.user.mesra!=null && $scope.user.name.length>=2)){
      
      var url=appLinkFactory.getUrl('updateUserProfile')+"?session_key="+sessionKey+"&secret_key="+secretKey;
      if($scope.user.name!=undefined){
        url+="&user_name="+$scope.user.name;
      }
      if($scope.user.mesra!=undefined){
        url+="&mesra_id="+$scope.user.mesra;
      }

      var req=httpRequestService.connect(url,'POST');
      req.success(function(data){
        if(data.code==200){
          alertify.success('Updated Profile Successfully');
        }
        else{
          alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
        }
      })
      req.error(function(err){
        //alertify.error('Please Check Internet Connectivity (or) No data is found');
      })
    }
    if($scope.user.name!=undefined && $scope.user.name.length<=2){
      alertify.error('Enter Valid Name');
      return false;
    }
    if(($scope.user.newPassword!=undefined && $scope.user.newPassword!=null) || ($scope.user.confirmPassword!=undefined && $scope.user.confirmPassword!=null) || ($scope.user.oldPassword!=undefined && $scope.user.oldPassword!=null)){
      if($scope.user.oldPassword==null){
        alertify.error('Enter Old Password');
        return false;
      }
      else if($scope.user.newPassword==null){
        alertify.error('Enter New Password');
        return false;
      }
      else if($scope.user.confirmPassword==null){
        alertify.error('Enter Confirm password');
        return false;
      }
      else if($scope.user.newPassword!=$scope.user.confirmPassword){
        alertify.error('New Password and Confirm Password do not match');
        return false;
      }

      var url=appLinkFactory.getUrl('updateUserPassword')+"?session_key="+sessionKey+"&secret_key="+secretKey+"&new_password="+$scope.user.newPassword+"&old_password="+$scope.user.oldPassword;
      var req=httpRequestService.connect(url,'POST');
      req.success(function(data){
        if(data.code==200){
          alertify.success('Updated Password Successfully');
        }
        else{
          alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
        }
      })
      req.error(function(err){
        //alertify.error('Please Check Internet Connectivity (or) No data is found');
      })
    }

    if($scope.response.file!=null && $scope.response.file!=undefined){
      var url=appLinkFactory.getUrl('updateUserProfile')+"?session_key="+sessionKey+"&secret_key="+secretKey;
      
      var data = new FormData();
      data.append("file", $scope.response.file);
      data.append("file_name", "sample.png");

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4 && (this.status==200||this.status==201)) {
          data=JSON.parse(this.responseText);
          if(data.code==200){
            alertify.success('Profile Image Updated Successfully');
            $scope.response={};
            $timeout(function(){$window.location.reload()},3000)
          }
          else{
            alertify.error(data.error)
          }
        }
        else if(this.readyState === 4){
          alertify.error(data.error)
        }
      });

      xhr.open("POST", url);

      xhr.send(data);
    }
  
  }


    //Game Points
  var getGamePoints=function(){
    var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
    var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
    var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
    
    var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid)+"?game_id="+gameId;
    var req=httpRequestService.connect(url,'GET');
    req.success(function(data){
      if(data.code==200){
        $scope.username=data.name;
        //$scope.points=data.points;
        $scope.mesra=data.meta?data.meta[0].value:"";
        $scope.user.name=$scope.username;
        $scope.user.mesra=$scope.mesra;
        $scope.avatarPhoto=data.avatarPhoto||"";
        $scope.photo={};
        $scope.photo['sm']=data.photoSmall;
        $scope.photo['lg']=data.photoLarge;
        if(data.photoLarge==data.avatarPhoto){
          $scope.avatarPhoto="";
        }
        $scope.level=1;
      }
      else{
        alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
      }
    })
    req.error(function(err,msg){
      //alertify.error('Please Check Internet Connectivity (or) No data is found');
    })
  }

  var init=function(){
    $scope.user={};
    $scope.avatarPhoto="";
    getGamePoints();
  }

  init();
	
}]);