'use strict';

angular.module('switch.team', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/team', {
    templateUrl: 'app/components/team/team.html',
    controller: 'teamController'
  });
}])

.controller('teamController', ['$scope','$location','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {

	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}
	
	$scope.goToProfile=function(id){
		$location.path('/profile').search('id',id);
	}
	
	//Team User List
	$scope.getTeamUsers=function(teamId){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		
		var url = appLinkFactory.getUrl('teamUsers').replace(new RegExp('{id}', 'g'), teamId) + "?pagenumber=" + $scope.teamsearch.teamPage + "&game_id=" + gameId + "&keywords=" + $scope.teamsearch.keywords;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				if($scope.teamsearch.teamPage==0){
					$scope.teamUsers=[];
				}
				$scope.teamUsers=$scope.teamUsers.concat(data.data);
				$scope.teamName=data.team.name;
				$scope.teamsearch.teamPage++;
				if(data.more){
					$scope.getTeamUsers(teamId);
				}
			}
			else{
				if(data.error=="Invalid session key"){
					appCookieFactory.clearValue('switchUserInfo');
				    appCookieFactory.clearValue('switchUserTeamInfo');
				    $location.path('/login');
				}
				else{
					$scope.teamUsers=[];
				}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Other User Profile
	var getOtherUserProfile=function(userId){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userId)+"?session_key="+sessionKey;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.overallPoints=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.achievements=data.itemPhotos?data.itemPhotos:[];

			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	

	var init=function(){
		$scope.teamUsers=[];
		$scope.achievements=[];	
		$scope.otherProfile=true;
		var teamId=$location.search().id;
		$scope.teamId=teamId;
		$scope.teamsearch={keywords:'',teamPage:0};
		$scope.getTeamUsers(teamId);
		
	}
	
	init();

}]);