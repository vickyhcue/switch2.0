'use strict';

angular.module('switch.video', ['ngRoute','ngAlertify','ngSanitize'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/video', {
    templateUrl: 'app/components/video/video.html',
    controller: 'videoController'
  });
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])

.controller('videoController', ['$scope','$location','$filter','$sce','$window','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,$filter,$sce,$window,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {
	$scope.trustSrc = function(src) {
    	return $sce.trustAsResourceUrl(src);
  	}
  	var challengeId=0;
	var challengeX=0;
	var challengeY=0;
	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}
	$scope.escapeHTML=function(text){
		return $sce.trustAsHtml(text);
	}
	$scope.showVideoChallenge = function(index){
		$scope.currentChallenge.showVideo=true;
		$timeout(function(){
			document.getElementById('videoSource'+index).play();
		},500);
	}
	//Video Complete Challenge
	$scope.videoComplete=function(){
		if(!$scope.islocked && $scope.isplayable){
			$scope.claimChallenge();
		}
	}

	//Go To User Profile Page
	$scope.gotoProfile=function(id){
		$location.path('/profile').search('id',id);
	}

	 $scope.refresh=function(){
  		$window.location.reload()
  	}
	$scope.videoTab=function(id){
		var index=0;
		$scope.games.forEach(function(e,i){
			if(e.id==id){
				index=i;
				return false;
			}
		});
		$scope.currentGame=$scope.games[index];
		getChallengesInGame($scope.games[index].id,index,0);
	}
	$scope.slideVideo=function(counter){
		var x=0;
		$scope.games.forEach(function(e,i){
			if(e.id==$scope.currentGame.id){
				x=i;
				return false;
			}
		});
		var y=0;var ind=0;
		$scope.currentGame.challenges.forEach(function(e,i){
			if(e.id==$scope.currentChallenge.id){
				y=i;
				return false;
			}
		});
		if(counter<0){
			if(y-1>=0){
				ind=y-1;
			}
			else{
				ind=$scope.currentGame.challenges.length-1;
			}
		}
		else if(counter>0){
			if(y+1<$scope.currentGame.challenges.length){
				ind=y+1;
			}
			else{
				ind=0;
			}
		}
		else{
			ind=y;
		}
		$scope.currentSlide=ind;
		$scope.currentChallenge=$scope.currentGame.challenges[ind];
		$location.path('/video').search('id',$scope.games[x].challenges[ind].id)
		getChallengeComments($scope.games[x].challenges[ind].id,x,ind);
	}

		//trigger photo
	$scope.uploadPhoto=function(){
		document.getElementById('challengePhoto').click();
	}

	//Like a challenge
	$scope.postLike=function(){
		var secretKey=CONFIG.apiKey;
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('challengeActivity');
		var param={
			"userid": userId,
			"challengeid":$scope.currentChallenge.id,
			"activitytype": "LIK",
			"createdby":userId,
			"active":!$scope.currentChallenge.ratedLike
		}	
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
			if(data.code==200){
				if(data.liked){

				$scope.currentChallenge.likeNo=parseInt($scope.currentChallenge.likeNo)+1;
					//alertify.success('You have liked the post');
				}
				else{

				$scope.currentChallenge.likeNo=parseInt($scope.currentChallenge.likeNo)-1;
					//alertify.success('You have unliked the post');
				}
				$scope.currentChallenge.ratedLike=data.liked;
			}
			else{
				alertify.error(data.error);
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Comment a challenge
	$scope.postComment=function(){
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		if ($scope.challengeComment == undefined || $scope.challengeComment == null || $scope.challengeComment == "") {
			alertify.error('Please enter text to post a comment');
			return false;
		}
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var content=$scope.challengeComment;
		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('challengeComment');
		var param={
			"userid":userId,
			"bundleid":gameId,
			"topicid":$scope.games[0].id,
			"challengeid":$scope.currentChallenge.id,
			"activitytype":"CMT",
			"activityid":null,
			"replyid":null,
			"commenttext":content,  
			"createdby":userId 
		}
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){

				$scope.slideVideo(0);
				$scope.challengeComment='';
				$scope.currentChallenge.commentNo=parseInt($scope.currentChallenge.commentNo)+1;
				
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Select a claim in challenge
	$scope.selectClaim=function(id,isSingle){
		if(isSingle){
			$scope.quiz_option_id=id;
		}
		else{
			var ind=$scope.quiz_option_id_list.indexOf(id);
			if(ind>=0){
				$scope.quiz_option_id_list.splice(ind,1);
			}
			else{
				$scope.quiz_option_id_list.push(id);
			}
		}
	}

	//Load More Comments
	$scope.nextPageComments=function(){
		$scope.commentsPageNo+=1;
		getChallengeComments($scope.currentChallenge.id,challengeX,challengeY,$scope.commentsPageNo);
	}

	//Accept a Challenge
	var allowChallenge=function(challengeId){
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var url=appLinkFactory.getUrl('allowChallenge');
		var param={
			'userid':userId,
			'challengeid':parseInt(challengeId)
		}
		var req=httpRequestService.connect(url,'POST',param);
		req.success(function(data){
			//Success
			$scope.islocked=data.islocked;
			$scope.isplayable=data.isplayable;
			if(data.islocked){
				$scope.completedMessage=data.message==null?"Challenge Completed":data.message;
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Claim a challenge
	$scope.claimChallenge=function(){
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		event.preventDefault ? event.preventDefault() : (event.returnValue = false);
		$scope.challenge=$scope.currentChallenge;
		var data;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url=appLinkFactory.getUrl('challengeComplete');
		var param = {
			"userid":userId,
			"challengeid":$scope.currentChallenge.id,
			"activitytype":"COM",
			"commenttext":"",
			"challengecompleted":true,
			"isaccepted":true,
			"challengetypeid":$scope.currentChallenge.challengeTypeId,
			"active": true,
			"pointsawarded": $scope.currentChallenge.points,
	    };
	    var header;
	    
	    
		    
		
	    
			var req=httpRequestService.connect(url,'POST',param);
			req.success(function(data){
				if(data.code==200){
					$scope.challenge.completionMessage=(data.successmsg!=undefined && data.successmsg.length>0)?data.successmsg:'';
					
					if(data.items){
						$scope.challenge.badges=data.items;
					}
					$scope.challengeReponse = data;
					$scope.popupmsg = $scope.currentChallenge.completeonmsg;
					document.getElementById('confirmation').click();
					
				}
				else{
					alertify.error('Please select an option to complete the challenge');
				}			
			})
			req.error(function(err,msg){
				//alertify.error('Please Check Internet Connectivity (or) No data is found');
			});
		
	}

	//get Index of challenge to highlight
	var getChallengeIndex=function(challenges,challengeId){
		var value;
		challenges.forEach(function(e,i){
		    if (e.id == parseInt(challengeId)) {
				value=i;
				return false;
			}
		})
		return value;
	}

	//Game Points
	var getGamePoints=function(){
		var userid=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('userProfile').replace(new RegExp('{id}','g'),userid);
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level=1;
			}
			else{
				alertify.error(data.error);
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Games By Category
	var getChallenge=function(id){
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');
		var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
		var url=appLinkFactory.getUrl('challenge').replace(new RegExp('{id}','g'),id)+"?bundleid="+gameId+"&user_id="+userId;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			//if(data.code==200){
				$scope.selectedChallenge=data;
				$scope.games=[data.quest];
				$scope.gameTitle = "";//data.quest.title;
				$scope.currentGame=$scope.games[0];
				getChallengesInGame(data.topicid, 0, 0, id);//data.quest.id
			//}
			//else{
			//	alertify.error(data.error);
			//}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}
	
	//Challenges in Game
	var getChallengesInGame=function(gameId,x,y,challengeId){
		if(challengeId==undefined){
			challengeId=0;
		}
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url = appLinkFactory.getUrl('topicChallenges').replace(new RegExp('{id}', 'g'), gameId) + "?isvideo=true";
		var req=httpRequestService.connect(url,'GET');
		challengeX=x;
		challengeY=y;
		req.success(function(data){
			if(data.code==200){
				
				$scope.games[x].challenges=data.data;
				$scope.slides=data.data;
				if(challengeId!=0)
					y=getChallengeIndex($scope.games[x].challenges,challengeId);
				else
					y=0;
				$scope.currentSlide=y;
				
				getChallengeComments($scope.games[x].challenges[y].id,x,y);
				//$scope.claimChallenge()
			}
			else{
				alertify.error(data.error);
			}

		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Comments in Challenge
	var getChallengeComments=function(gameId,x,y,page){
		if(page==undefined){
			page=0;
		}
		challengeX=x;
		challengeY=y;
		$scope.commentsPageNo=page;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionkey');
		var url = appLinkFactory.getUrl('commentlist').replace(new RegExp('{id}', 'g'), gameId) + "?pagenumber=" + page;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				if(page==0){$scope.games[x].challenges[y]['comments']=[];}
				$scope.games[x].challenges[y].disableSubmit=true;
				$scope.games[x].challenges[y]['comments']=$scope.games[x].challenges[y]['comments'].concat(data.data);
				var currentComments = $scope.games[x].challenges[y]['comments'];
				$scope.currentChallenge=$scope.games[x].challenges[y];
				if($scope.currentChallenge.footnote!=null){
					$scope.currentChallenge.footnote=$scope.escapeHTML($scope.currentChallenge.footnote.replace(/href="([0-9]*)"/g,'href="#/challenge?id='+'$1'+'"'));	
				}
				
				if($scope.currentChallenge.id==$scope.selectedChallenge.id){
				    $scope.currentChallenge = $scope.selectedChallenge;
				    $scope.currentChallenge['comments'] = currentComments;
				}
				$scope.currentChallenge.showVideo=false;
				$scope.showMoreComments=data.more;
			}
			else{
				$scope.games[x].challenges[y]['comments']=[];
				$scope.games[x].challenges[y].disableSubmit=true;
				$scope.currentChallenge=$scope.games[x].challenges[y];
				$scope.currentChallenge.footnote=$scope.escapeHTML($scope.currentChallenge.footnote.replace(/href="([0-9]*)"/g,'href="#/challenge?id='+'$1'+'"'));
				$scope.currentChallenge.showVideo=false;
				$scope.showMoreComments=false;
			}
			challengeInit();

		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenge Init
	var challengeInit=function(){
		//$scope.challenge.enableSubmit=false;
		//$scope.challenge.blurText=true;
		$scope.completedMessage="Challenge Completed"
		$scope.response={};
		$scope.challengeConfig={
			'showSubmitBtn':true,
			'disableSubmitBtn':false,
			'blurPdf':false,
			'blurText':false,
			'isSubmit':true,
			'pdfLoaded':false
		};
		allowChallenge(challengeId);
		//($scope.challenge.repeat||!$scope.challenge.claimed)&&($scope.challenge.challengeTypeId==1||$scope.challenge.quizOptions.length>1)
		if(
			($scope.currentChallenge.claimed&&!$scope.currentChallenge.repeat) ||
			(  (!$scope.currentChallenge.claimed||$scope.currentChallenge.repeat) &&
				($scope.currentChallenge.challengeTypeId!=1 && $scope.currentChallenge.medias!=undefined && $scope.currentChallenge.medias[0].type!='Website' &&
					($scope.currentChallenge.quizOptions==undefined || ($scope.currentChallenge.quizOptions!=undefined && $scope.currentChallenge.quizOptions.length==1))
			//($scope.challenge.challengeTypeId==1) || //Photo Challenge
	        //($scope.challenge.medias!=undefined && $scope.challenge.quizOptions.length==1) || //Flash Card
			//($scope.challenge.quizOptions!=undefined && $scope.challenge.quizOptions.length>1) )//Multiple Option
			))){
			$scope.challengeConfig.showSubmitBtn=false;
		}

		if($scope.currentChallenge.challengeTypeId!=1 && $scope.currentChallenge.medias==undefined && ($scope.currentChallenge.quizOptions==undefined || ($scope.currentChallenge.quizOptions!=undefined && $scope.currentChallenge.quizOptions.length==1))){
			$scope.challengeConfig.isSubmit=false;
		}
		
		//Disable Button
		//(!$scope.challenge.enableSubmit&&$scope.challenge.medias!=undefined&&$scope.challenge.medias[0].type!='Website') || ($scope.challenge.medias==undefined && ($scope.challenge.quizOptions==undefined || $scope.challenge.quizOptions.length==1) && $scope.challengeConfig.blurText&& !($scope.challenge.challengeTypeId==1 && $scope.challenge.medias!=undefined && $scope.challenge.medias[0].type=='Website'))
		if(
			//(($scope.challenge.medias==undefined || ($scope.challenge.medias!=undefined && $scope.challenge.medias[0].type=='Website')) && $scope.challenge.quizOptions==undefined && $scope.challenge.challengeTypeId==1)
			($scope.currentChallenge.challengeTypeId!=1 && $scope.currentChallenge.medias!=undefined && ($scope.currentChallenge.medias[0].type=='PDF' ||$scope.currentChallenge.medias[0].type=='Video') && ($scope.currentChallenge.quizOptions==undefined || ($scope.currentChallenge.quizOptions!=undefined && $scope.currentChallenge.quizOptions.length==1)))
			){
			$scope.challengeConfig.disableSubmitBtn=true;
		}
		//$scope.challenge.medias==undefined && ($scope.challenge.quizOptions==undefined || $scope.challenge.quizOptions.length==1) && !$scope.challenge.claimed && !($scope.challenge.challengeTypeId==1 )
		if((!$scope.currentChallenge.claimed||$scope.currentChallenge.repeat) && $scope.currentChallenge.medias!=undefined && $scope.currentChallenge.medias[0].type=='PDF' ){ // Blur PDF
			$scope.challengeConfig.blurPdf=true;
		}
		if((!$scope.currentChallenge.claimed||$scope.currentChallenge.repeat) && $scope.currentChallenge.medias==undefined && 
		   ($scope.currentChallenge.quizOptions==undefined || ($scope.currentChallenge.quizOptions!=undefined && $scope.currentChallenge.quizOptions.length==1)) && $scope.currentChallenge.challengeTypeId!=1
		   && ($scope.currentChallenge.challengeTypeId!=4 && $scope.currentChallenge.challengeType!='non-interactive')
		   ){ //Flash Card
			//$scope.challengeConfig.blurText=true;
		}

		if($scope.currentChallenge.medias!=undefined && $scope.currentChallenge.medias[0].type=="PDF"){
			$scope.challengeConfig.pdfLoaded=true;
			// var pdftimeout=$interval(function(){
			// 	if($('iframe').contents()!=undefined && !$('iframe').contents().find('#loadingBar').is(':visible') ){
			// 		$interval.cancel(pdftimeout);
			// 		$scope.challengeConfig.pdfLoaded=true;
			// 	}
			// },5000);
		}
	}

	$scope.getDate = function (Visitedate) {
	    if (Visitedate !== undefined) {
	        var m = moment(new Date(), "YYYYMMDD");
	        var date = moment(Visitedate, "YYYYMMDD HH:mm");


	        var years = m.diff(date, 'years');
	        m.add(-years, 'years');
	        var months = m.diff(date, 'months');
	        m.add(-months, 'months');
	        var days = m.diff(date, 'days');
	        m.add(-days, 'days');
	        var hours = m.diff(date, 'hours');
	        m.add(-hours, 'hours');
	        var minutes = m.diff(date, 'minutes');
	        var daysago;
	        if (years <= 0) {
	            if (months <= 0) {
	                if (days <= 0) {
	                    if (hours == 0) {
	                        daysago = minutes.toString() + " minutes ago ";
	                    }
	                    else {
	                        daysago = hours.toString() + " Hours " + minutes.toString() + " minutes ago ";
	                    }

	                } else {
	                    daysago = days.toString() + " Day(s) Ago ";
	                }
	            } else {
	                if (months > 1) {
	                    daysago = months.toString() + " Months " + days.toString() + " Day(s) " + " ago ";
	                } else {
	                    daysago = months.toString() + " Month " + days.toString() + " Day(s)" + " ago ";
	                }
	            }
	        } else {
	            daysago = $filter('date')(new Date(Visitedate), "dd-MMM-yyyy");
	        }
	        return daysago;
	    }
	    else {

	    }
	}

	var init=function(){
		$scope.challengeReponse={};
		$scope.quiz_option_id_list=[];
		$scope.commentsPageNo=1;
		challengeId = $location.search().id;
		var topicid = $location.search().topicid;
		$scope.nextbtn = true;
		$scope.prevbtn = true;
		//acceptChallenge(challengeId);
		
		if(!challengeId && challengeId==null)
			$location.path('/videos');
		getGamePoints();
		getChallenge(challengeId);		
		

		
	}
	
	init();

	$scope.challengecheck = function (chlg) {
	        var path1 = $location.$$absUrl;
	        path1 = path1.substr(0, (path1.indexOf('#') + 1));
	        window.location.href = path1 + "/video?id=" + $scope.nextvalId + '&topicid=' + chlg.topicid;	  
	}
	$scope.challengeprevcheck = function (chlg) {
	    var path1 = $location.$$absUrl;
	    path1 = path1.substr(0, (path1.indexOf('#') + 1));
	    window.location.href = path1 + "/video?id=" + $scope.prevvalId + '&topicid=' + chlg.topicid;
	}

}]);