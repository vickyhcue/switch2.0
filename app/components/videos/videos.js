'use strict';

angular.module('switch.videos', ['ngRoute','ngAlertify'])

.config(['$routeProvider','$sceDelegateProvider', function($routeProvider,$sceDelegateProvider) {
  $routeProvider.when('/videos', {
    templateUrl: 'app/components/videos/videos.html',
    controller: 'videosController'
  });
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://switch.petronas.com/**'
  ]);
}])

.controller('videosController', ['$scope','$location','$filter','appCookieFactory','appLinkFactory','httpRequestService','CONFIG','$timeout','alertify',
	function($scope,$location,$filter,appCookieFactory,appLinkFactory,httpRequestService,CONFIG,$timeout,alertify) {
	
	var challengeX=0;
	var challengeY=0;
	$scope.logout=function(){
		appCookieFactory.clearValue('switchUserInfo');
		$location.path('/login');
	}
	$scope.gotoChallenge=function(challenge,gameId){
		//return false;
		var videoPos=[];
		angular.forEach($scope.games,function(e,i){
			videoPos.push($('#carousel'+i+' .owl-item').index($('#carousel'+i).find('.active')[0]));
		});
		videoPos.push($(window).scrollTop());
		appCookieFactory.setValue('switchVideoPosition',JSON.stringify(videoPos));
		$location.path('/video').search('id',challenge.id);
	}

	$scope.videoTab=function(id){
		var index=0;
		$scope.games.forEach(function(e,i){
			if(e.id==id){
				index=i;
				return false;
			}
		});
		$scope.currentGame=$scope.games[index];
		getChallengesInGame($scope.games[index].id,index,1);
	}
	$scope.slideVideo=function(counter){
		var x=0;
		$scope.games.forEach(function(e,i){
			if(e.id==$scope.currentGame.id){
				x=i;
				return false;
			}
		});
		var y=0;var ind=0;
		$scope.currentGame.challenges.forEach(function(e,i){
			if(e.id==$scope.currentChallenge.id){
				y=i;
				return false;
			}
		});
		if(counter<0){
			if(y-1>=0){
				ind=y-1;
			}
			else{
				ind=$scope.currentGame.challenges.length-1;
			}
		}
		else if(counter>0){
			if(y+1<$scope.currentGame.challenges.length){
				ind=y+1;
			}
			else{
				ind=0;
			}
		}
		else{
			ind=y;
		}
		$scope.currentSlide=ind;
		$scope.currentChallenge=$scope.currentGame.challenges[ind];
		getChallengeComments($scope.games[x].challenges[ind].id,x,ind);
	}

	//Comment a challenge
	$scope.postLike=function(){
		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('rate')+"?session_key="+sessionkey+"&type=challenge&type_id="+$scope.currentChallenge.id;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.slideVideo(0);
				$scope.challengeComment='';
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Comment a challenge
	$scope.postComment=function(){
		var secretKey=CONFIG.apiKey;
		var sessionkey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		$scope.sessionkey = sessionkey;
		var url=appLinkFactory.getUrl('challengeComment')+"?session_key="+sessionkey+"&secret_key="+secretKey+"&type_id="+$scope.currentChallenge.id+"&type=challenge&content="+$scope.challengeComment;
		var req=httpRequestService.connect(url,'POST');
		req.success(function(data){
			if(data.code==200){
				$scope.slideVideo(0);
				$scope.challengeComment='';
				$scope.currentChallenge.commentNo+=1;		
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}

	//Load More Comments
	$scope.nextPageComments=function(){
		$scope.commentsPageNo+=1;
		getChallengeComments($scope.currentChallenge.id,challengeX,challengeY,$scope.commentsPageNo);
	}

	var trendvideos = function () {
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');

	    var url = appLinkFactory.getUrl('trendingvideo').replace(new RegExp('{id}', 'g'), gameId);
	    var req = httpRequestService.connect(url, 'GET');
	    req.success(function (data) {
	        if (data.code == 200) {
	            $scope.trendinglist = data.data[0];
	            getGamePoints();
	        }
	    })
	    req.error(function (err, msg) {
	     
	    })
	}

	//Game Points
	var getGamePoints=function(){
	    var gameId = appCookieFactory.getJsonValue('switchUserInfo', 'gameId');
	    var userid = appCookieFactory.getJsonValue('switchUserInfo', 'userId');
	    var sessionkey = appCookieFactory.getJsonValue('switchUserInfo', 'sessionKey');

	    var url = appLinkFactory.getUrl('topicChallengeList').replace(new RegExp('{id}', 'g'), gameId) + "?user_id=" + userid + "&excludeId=" + $scope.trendinglist.id + "&isvideo=true";
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
		    if (data.code == 200) {
		        $scope.topiclist = $scope.games.concat(data.data);		      
				$scope.username=data.name;
				$scope.points=data.points;
				$scope.photo={};
				$scope.photo['sm']=data.photoSmall;
				$scope.photo['lg']=data.photoLarge;
				$scope.level = 1;

				$timeout(function () {

				    $('.owl-carousel').owlCarousel({
				        loop: false,
				        margin: 20,
				        dots: false,
				        nav: true,
				        navText: [
                          "<span class='prev-icon'></span>",
                          "<span class='next-icon'></span>"
				        ],
				        autoplay: false,
				        autoplayTimeout: 2000,
				        autoplayHoverPause: true,
				        responsive: {
				            0: {
				                items: 2
				            },
				            600: {
				                items: 3
				            },
				            1000: {
				                items: 3
				            }
				        }
				    });
				    $('.owl-carousel-trend').owlCarousel({
				        loop: false,
				        margin: 20,
				        dots: true,
				        nav: false,

				        autoplay: false,
				        autoplayTimeout: 2000,
				        autoplayHoverPause: true,
				        responsive: {
				            0: {
				                items: 1
				            },
				            600: {
				                items: 1
				            },
				            1000: {
				                items: 1
				            }
				        }
				    });

				    $timeout(function () {
				        $scope.showSlider = true;
				        var videoPos = appCookieFactory.getValue('switchVideoPosition');
				        if (videoPos != undefined) {
				            videoPos = JSON.parse(videoPos);
				            $scope.games.forEach(function (e, i) {
				                $('#carousel' + i).trigger('to.owl.carousel', [videoPos[i], 0, true]);
				            });
				            $(window).scrollTop(videoPos[$scope.games.length]);
				        }
				    }, 100);
				}, 100);
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		})
	}
	

	//Games By Category
	var getGames=function(){

		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('games')+"?category_id=1&session_key="+sessionKey+"&page="+$scope.gamesPageNo;
		var req=httpRequestService.connect(url,'GET');
		req.success(function(data){
			if(data.code==200){
				$scope.games=$scope.games.concat(data.data);
				if(data.more==true){
					getGames(++$scope.gamesPageNo);
				}
				if(data.more==false){
					$scope.currentGame=$scope.games[0];
					$scope.games.forEach(function(e,i){
						$scope.games[i].challenges=[];
						$scope.games[i].pageNo=1;
						getChallengesInGame(e.id,i,1);
					})
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}
		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}

	//Challenges in Game
	var getChallengesInGame=function(gameId,x,offset){
		var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
		var url=appLinkFactory.getUrl('challengesInGame').replace(new RegExp('{id}','g'),gameId)+"?session_key="+sessionKey+"&page="+offset;
		var req=httpRequestService.connect(url,'GET');
		challengeX=x;
		req.success(function(data){
			if(data.code==200){
				$scope.games[x].challenges=$scope.games[x].challenges.concat(data.data);
				if(data.more){
					getChallengesInGame(gameId,x,++$scope.games[x].pageNo)
				}
				else{
					$scope.gamesCount++;
					if($scope.gamesCount==$scope.games.length){
						$scope.trending=$scope.games[0];
						$scope.games.splice(0,1);
					$timeout(function(){
						
							$('.owl-carousel').owlCarousel({
					          loop: false,
					          margin: 20,
					          dots: false,
					          nav: true,
					          navText: [
					            "<span class='prev-icon'></span>",
					            "<span class='next-icon'></span>"
					          ],
					          autoplay: false,
					          autoplayTimeout: 2000,
					          autoplayHoverPause: true,
					          responsive: {
					            0: {
					              items: 2
					            },
					            600: {
					              items: 3
					            },
					            1000: {
					              items: 3
					            }
					          }
					        });
					        $('.owl-carousel-trend').owlCarousel({
					          loop: false,
					          margin: 20,
					          dots: true,
					          nav: false,

					          autoplay: false,
					          autoplayTimeout: 2000,
					          autoplayHoverPause: true,
					          responsive: {
					            0: {
					              items: 1
					            },
					            600: {
					              items: 1
					            },
					            1000: {
					              items: 1
					            }
					          }
					        });

					        $timeout(function(){
					       		$scope.showSlider=true;
					       		var videoPos=appCookieFactory.getValue('switchVideoPosition');
					       		if(videoPos!=undefined){
					       			videoPos=JSON.parse(videoPos);
						       		$scope.games.forEach(function(e,i){
						       			$('#carousel'+i).trigger('to.owl.carousel',[videoPos[i],0,true]);
						       		});
						       		$(window).scrollTop(videoPos[$scope.games.length]);
					       		}
					       	},100);
						},100);
					}
				}
			}
			else{
				alertify.error(data.error);
if(data.error=="Invalid session key"){
	appCookieFactory.clearValue('switchUserInfo');
    appCookieFactory.clearValue('switchUserTeamInfo');
    $location.path('/login');
}
			}

		})
		req.error(function(err,msg){
			//alertify.error('Please Check Internet Connectivity (or) No data is found');
		});
	}
	var init=function(){
		$scope.commentsPageNo=1;
		$scope.gamesPageNo=1;
		$scope.games=[];
		trendvideos();
		//getGames($scope.gamesPageNo);		
		$scope.showSlider=false;
		$scope.gamesCount=0;
	}
	
	init();

}]);