switchApp.directive('headerMenu', function () {
        var headerMenu={};
        headerMenu.restrict="E";
        headerMenu.template="<li ng-repeat='menu in menus'><a ng-href='#/{{menu}}'>{{menu}}</a></li>";
        headerMenu.scope={
        	menus:"=menus"
        }

        return headerMenu;
    });
