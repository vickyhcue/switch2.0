switchApp.directive("switchlink", [function() {
    return {
      restrict:'A',
      scope: {
        ngBindHtml: "=",
        filepreview: "="
      },
      link: function(scope, element, attributes) {
        if(scope.ngBindHtml && scope.ngBindHtml.replace){
          scope.ngBindHtml=scope.ngBindHtml.replace(/href="([0-9]*)"/g,'href="#/challenge?id='+'$1'+'"');
        }
      }
    }
  }]);
