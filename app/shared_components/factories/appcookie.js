switchApp.factory('appCookieFactory', ['$cookies',function ($cookies) {
        var appCookie={};
        appCookie.setValue=function(key,value){
            $cookies.put(key,value);
        }
        appCookie.getValue=function(key){
            return $cookies.get(key);
        }
        appCookie.clearValue=function(key){
            $cookies.remove(key)
        }
        appCookie.getJsonValue=function(name,key){
            if($cookies.get(name)){
                return JSON.parse($cookies.get(name))[key];
            }
            else{
                return $cookies.get(name);
            }
        }
        return appCookie;
    }]);
