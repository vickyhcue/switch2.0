switchApp.factory('appLinkFactory', function (CONFIG) {
        var url={};
        url.baseUrl={
            //"dev":"https://switchtest.gametize.com/api",
            //"dev": "http://192.168.137.1:8013/api",
            //"dev": "http://192.168.0.121:8013/api",
            "dev": "http://switch-2-app-lb-1817246789.ap-south-1.elb.amazonaws.com/switch/api",
        	"staging": "http://switch-2-app-lb-1817246789.ap-south-1.elb.amazonaws.com/cms/api",
        	"prod": "https://switch.petronas.com/api"
        }
        url.endPoint={
            "login": "/authenticate/login",
            "userProfile": "/profile/users/{id}",
            "latestChallenge": "/dashboardapi/categories_challenge_latest",
            "leaderboard": "/dashboardapi/leaderboard_overview",
            "leaderboardTeam": "/dashboardapi/leaderboard_overviewteam",
            "leaderboardCsg": "/dashboardapi/leaderboard_overviewgroup",
            "leaderboardPage": "/dashboardapi/leaderboard_overviewpaging",
            "leaderboardTeamPage": "/dashboardapi/leaderboard_overviewteampaging",
            "leaderboardCsgPage": "/dashboardapi/leaderboard_overviewgrouppaging",
            "categoryTopics": "/profile/categorytopic/{id}",
            "topicChallenges": "/profile/topicchallenges/{id}",
            "activities": "/profile/activity/{id}",
            "carousel": "/dashboardapi/caruosel/{id}",
            "updatePassword": "/userlogin/updatepassword",
            "feedbackTopic": "/feedback/feedbacktopiclist",
            //"feedbackRating": "/feedback/ratingstypelist",
            "feedbackAdd": "/feedback/add",
            "activitycomment": "/challengeuseractivity/add",
            "commentlist": "/profile/challengecomment/{id}",
            "imageactivity": "/profile/activity/{id}",
            "carousellist": "/dashboardapi/caruosel",
            "profileimageupload": "/user/update/{id}",
            "FileUpload": "/file/upload",
            "challenge": "/profile/challenge/{id}",
            "challengeClaims":"/profile/activity/{id}",
            "challengeActivity": "/challengeuseractivity/useractivity",
            "challengeComment": "/completedcomments/add",
            "challengeComplete": "/challengeuseractivity/add",
            "allowChallenge": "/challengeuseractivity/canuserpalythischallenge",
            "forgotPassword": "/userlogin/forgotPassword",
            "verifyOtp": "/userlogin/verifyotp",
            "resetpwd": "/userlogin/resetpassword",
            "signup": "/userlogin/signinuser",
            "useractivityAdd": "/bundleuseractivity/add",
            "teamUsers": "/profile/teams/{id}",
            "activitycommentlist": "/profile/challengecompletioncomments/{id}",
            "topicChallengeList": "/edge/usertopicchallenges/{id}",
            "trendingvideo": "/edge/trending_videos/{id}",
            "completionCount": "/edge/categorycompletioncounts/{id}",
            "userfollow": "/userfollow/addupdateuserfollow",
            "follower": "/edge/getuserfollowers/{id}",
            "following": "/edge/getuserfollowing/{id}",
            "logout": "/authenticate/logout",
            "feedbacksetting": "/feedbacksettings/feedbackcategory/list/{id}",
            "feedbackRating": "/feedback/ratingstype/list/{id}",
            "iLikeIwish":"/feedbacksettings/ilikeiwish/{id}",
            "listcomments":"/bundleuseractivity/comments/{id} ",
            "feedbacksettings":"/feedbacksettings/list"
        }
        url.getUrl=function(module){
        	return url.baseUrl[CONFIG.environment] + url.endPoint[module];
        }
        return url;
    });
