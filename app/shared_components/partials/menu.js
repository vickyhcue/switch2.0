
angular.module('switch.menu', ['ngRoute','ngAlertify'])
.controller('menuController', ['$scope','$location','$window','$timeout','appCookieFactory','appLinkFactory','CONFIG','httpRequestService','alertify',
    function($scope,$location,$window,$timeout,appCookieFactory,appLinkFactory,CONFIG,httpRequestService,alertify) {
        
        var init=function(){
        	$scope.unCompletedVideos=0;
           	$scope.unCompletedChallenges=0;
        	
        	var userId=appCookieFactory.getJsonValue('switchUserInfo','userId');
			var sessionKey=appCookieFactory.getJsonValue('switchUserInfo','sessionKey');
			var gameId=appCookieFactory.getJsonValue('switchUserInfo','gameId');

			var url = appLinkFactory.getUrl('completionCount').replace(new RegExp('{id}', 'g'), gameId) + "?user_id=" + userId;
			var req=httpRequestService.connect(url,'GET');
			req.success(function(data){
				if(data.code==200){
					angular.forEach(data.data,function(e,i){
					    if (e.category == "Videos") {
							$scope.unCompletedVideos=e.noOfChallenges-e.noOfCompleted;
						}
					    else if (e.category == "Challenge") {
							$scope.unCompletedChallenges=e.noOfChallenges-e.noOfCompleted;
						}
					})					
				}
				else{
					alertify.error(data.error);
				}
			})
			req.error(function(err,msg){
				//alertify.error('Please Check Internet Connectivity (or) No data is found');
			})
        }
        init();
    }]);
