
angular.module('switch.tour', ['ngRoute','ngAlertify'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/tour', {
      templateUrl: 'app/shared_components/partials/tour.html',
      controller: 'tourController'
    });
  }])

.controller('tourController', ['$scope','$location','$window','$timeout','appCookieFactory','appLinkFactory','CONFIG','alertify',
    function($scope,$location,$window,$timeout,appCookieFactory,appLinkFactory,CONFIG,alertify) {
       
      var frompage=$location.search().page;

       $scope.closetour = function(){           
        appCookieFactory.setValue('switchfirstlogin',false);
        if(frompage == 'help'){
          $location.path('/help');
        }
        else{
          $location.path('/home');
        }
      
       }

  
       setTimeout(function(){
        $('.flexslider').flexslider({
          animation: "slide",
          slideshow: true,
          slideshowSpeed: 5000,
          animationLoop: false,
          pauseOnHover: true
      });
     }, 100);
        

    }]);
