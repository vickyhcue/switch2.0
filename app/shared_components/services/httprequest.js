	switchApp.run(function($http){
		//$http.defaults.headers.common.post={};
		//$http.defaults.headers.post={'content-type':'application/x-www-form-urlencoded'}
	    //$http.defaults.headers.post={'content-type':'multipart/form-data'}
	    $http.defaults.headers.post = { 'content-type': 'application/json' }
	});
	switchApp.factory('httpInterceptor', function ($q, $rootScope, $log, appCookieFactory, $injector, appLinkFactory) {
        var numLoadings = 0;

        return {
            request: function (config) {
                if (appCookieFactory.getValue('switchUserInfo')) {
                    config.headers['x-access-token'] = appCookieFactory.getJsonValue('switchUserInfo', 'sessionkey');
                }
                numLoadings++;
                $('#app-loader').addClass('show');
                return config || $q.when(config)

            },
            response: function (response) {
                if (response.config.loaderStatus == undefined) {
                    if ((--numLoadings) === 0) {
                        // Hide loader
                        $('#app-loader').removeClass('show');
                    }
                }
                return response || $q.when(response);

            },
            responseError: function (response) {
                if (response.config.loaderStatus == undefined) {
                    if (!(--numLoadings)) {
                        // Hide loader
                        $('#app-loader').removeClass('show');
                    }
                }
                return $q.reject(response);
            }
        };
    });
	switchApp.service('httpRequestService', function ($http) {
	    this.connect = function (url, method, data, uploadCheck) {
	        if (method == "POST") {
	            if (uploadCheck != undefined && uploadCheck == "UPLOAD") {
	                return $http.post(url, data, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } });
	            }
	            else {
	                return $http.post(url, data);
	            }
	        }
	        else if (method == "PUT") {
	            return $http.put(url, data);
	        } else if (method == "DELETE") {
	            return $http.delete(url);
	        }
	        else {
	            return $http.get(url);
	        }
	    }
    });
     switchApp.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('httpInterceptor');
     }]);


